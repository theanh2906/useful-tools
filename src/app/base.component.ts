import { Component, inject, signal } from '@angular/core';
import { MessageService } from 'primeng/api';
import { AuthService } from './auth/auth.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { UtilsService } from './services/utils.service';
import { AppState } from './reducers';
import { Store } from '@ngrx/store';

@Component({
  selector: 'app-base',
  standalone: true,
  template: '',
})
export class BaseComponent {
  protected messageService = inject(MessageService);
  protected authService = inject(AuthService);
  protected snackBar = inject(MatSnackBar);
  protected utils = inject(UtilsService);
  protected store = inject(Store<AppState>);
  protected data = signal<any | any[]>([]);
}
