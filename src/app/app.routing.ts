import { Routes } from '@angular/router';
import { InvoiceComponent } from './pages/invoice/invoice.component';
import { AuthGuard } from './auth/auth.guard';
import { ChangeCaseComponent } from './pages/change-case/change-case.component';
import { NotesComponent } from './pages/notes/notes.component';
import { MyCalendarComponent } from './pages/calendar/my-calendar.component';
import { UrlComponent } from './pages/url/url.component';
import { AuthenticationComponent } from './pages/authentication/authentication.component';
import { StorageComponent } from './pages/storage/storage.component';
import { TimeCalculatorComponent } from './pages/time-calculator/time-calculator.component';
import { WeatherComponent } from './pages/weather/weather.component';
import { QrGeneratorComponent } from './pages/qr-generator/qr-generator.component';
import { ZipComponent } from './pages/zip/zip.component';
import { BabyComponent } from './pages/baby/baby.component';
import { MonitorComponent } from './pages/monitor/monitor.component';

export const routes: Routes = [
  {
    path: 'invoice',
    component: InvoiceComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'change-cases',
    component: ChangeCaseComponent,
  },
  {
    path: 'notes',
    component: NotesComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'calendar',
    component: MyCalendarComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'url',
    component: UrlComponent,
  },
  {
    path: 'auth',
    component: AuthenticationComponent,
  },
  {
    path: 'storage',
    component: StorageComponent,
  },
  {
    path: 'time-calculator',
    component: TimeCalculatorComponent,
  },
  {
    path: 'weather',
    component: WeatherComponent,
  },
  {
    path: 'barcode',
    component: QrGeneratorComponent,
  },
  {
    path: 'zipping',
    component: ZipComponent,
  },
  {
    path: 'baby',
    component: BabyComponent,
  },
  {
    path: 'monitor',
    component: MonitorComponent,
  },
  {
    path: ':token',
    component: ChangeCaseComponent,
  },
];
