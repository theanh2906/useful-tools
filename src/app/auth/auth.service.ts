import { EventEmitter, inject, Injectable, OnDestroy } from '@angular/core';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { User } from './user.model';
import { map, tap } from 'rxjs/operators';
import { environment } from '../../environments/environment';
import { CONSTANTS } from '../shared/constants';
import { BaseService } from '../services/base.service';
import { ActivatedRoute } from '@angular/router';
import {
  AuthData,
  AuthResponseData,
  LoginConfig,
  LoginMethod,
} from '../shared/models';

@Injectable({
  providedIn: 'root',
})
export class AuthService extends BaseService implements OnDestroy {
  public isAzureUser = new BehaviorSubject<boolean>(false);
  public azureToken = new BehaviorSubject<string>('');
  public facebookToken = new BehaviorSubject<string>('');
  public googleToken = new BehaviorSubject<string>('');
  public authData$ = new BehaviorSubject<any>(this.authData);
  isLogin = true;
  public tokenRefresh = new EventEmitter<any>();
  // @ts-ignore
  private activeLogoutTimer: any;
  private route = inject(ActivatedRoute);

  constructor() {
    super();
  }

  get userId() {
    return this._subject.asObservable().pipe(
      map((user) => {
        if (user) {
          return user.id;
        }
        return null;
      })
    );
  }

  get authenticated() {
    return this._subject.getValue() != null;
  }

  get isAuthenticated() {
    return this._subject.asObservable().pipe(
      map((user) => {
        if (user) {
          return !!user.token;
        }
        return (
          !!sessionStorage.getItem(CONSTANTS.AUTH_TOKEN_KEY) ||
          !!sessionStorage.getItem('azureToken')
        );
      })
    );
  }

  get token() {
    return this._subject.asObservable().pipe(
      map((user) => {
        if (user) {
          return user.token;
        }
        return null;
      })
    );
  }

  get authData(): AuthData | null {
    if (sessionStorage.getItem('authData')) {
      return JSON.parse(
        sessionStorage.getItem('authData') as string
      ) as AuthData;
    } else {
      return null;
    }
  }

  ngOnDestroy(): void {}

  login = (config: LoginConfig): any => {
    switch (config.method) {
      case LoginMethod.CREDENTIAL:
        return this.http
          .post<AuthResponseData>(
            `https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=${environment.webApiKey}`,
            {
              email: config.email,
              password: config.password,
              returnSecureToken: true,
            }
          )
          .pipe(
            tap(this.setUserData.bind(this)),
            tap((res) => {
              this.autoLogin(+res.expiresIn).subscribe();
            })
          );
      case LoginMethod.GOOGLE:
        if (config.googleUser) {
          const authData = config.googleUser.getAuthResponse(true);
          this.setUserData({
            email: config.googleUser.getBasicProfile().getEmail(),
            expiresIn: `${authData.expires_in}`,
            idToken: authData.id_token,
            localId: config.googleUser.getBasicProfile().getId(),
          });
          this.autoLogin(config.expiredIn).subscribe();
        }
        return null;
    }
  };

  autoLogin = (expiredIn: number) => {
    return of(window.sessionStorage.getItem('authData')).pipe(
      map((storedData) => {
        if (!storedData) {
          return;
        }
        const parsedData = JSON.parse(storedData) as AuthData;
        const expirationTime = new Date(parsedData.expiresIn);
        if (expirationTime <= new Date()) {
          return;
        }
        return new User(
          parsedData.userId,
          parsedData.email,
          parsedData.token,
          expiredIn
        );
      }),
      tap((user) => {
        if (user) {
          this._subject.next(user);
          this.autoLogout(expiredIn);
        }
      }),
      map((user) => {
        return !!user;
      })
    );
  };

  signup = (email: string, password: string) => {
    return this.http
      .post<AuthResponseData>(
        `https://identitytoolkit.googleapis.com/v1/accounts:signUp?key=${environment.webApiKey}`,
        { email, password, returnSecureToken: true }
      )
      .pipe(tap(this.setUserData.bind(this)), tap(this.saveUser.bind(this)));
  };

  logout = () => {
    sessionStorage.removeItem('authData');
    this._subject.next(null);
  };

  validateToken() {
    if (this.authData) {
      this._subject.next(this.authData);
      // const expiredIn = this.authData?.expiresIn;
      // if (expiredIn < Date.now()) {
      //   sessionStorage.clear();
      //   // @ts-ignore
      //   this._subject.next(null);
      // } else {
      //   this.autoLogout(expiredIn);
      //   // @ts-ignore
      //   this._subject.next(this.authData);
      // }
    }
  }

  autoLogout = (duration: number) => {
    if (this.activeLogoutTimer) {
      clearTimeout(this.activeLogoutTimer);
    }
    this.activeLogoutTimer = setTimeout(
      this.logout,
      duration * 1000 ?? 360000000
    );
  };

  saveUser(user: AuthResponseData) {
    return this.http
      .post(`${this.utils.getRemoteApiUrl()}/auth/add-user`, user)
      .subscribe(console.log);
  }

  authenticate = (email: string, password: string, expiredIn: number) => {
    this.utils.isLoading.next(true);
    let authObs: Observable<AuthResponseData>;
    if (this.isLogin) {
      authObs = this.login({
        email,
        password,
        method: LoginMethod.CREDENTIAL,
        expiredIn: expiredIn ?? 0,
      });
    } else {
      authObs = this.signup(email, password);
    }
    authObs.subscribe(
      (res) => {
        if (this.route.snapshot.queryParams.ref) {
          this.router.navigateByUrl(this.route.snapshot.queryParams.ref);
        } else {
          this.router.navigateByUrl('calendar');
        }
      },
      (error) => {
        const errorCode = error.error.error.message;
        switch (errorCode) {
          case 'INVALID_PASSWORD':
            this.utils.showErrorMessage('This password is not correct.');
            break;
          case 'EMAIL_NOT_FOUND':
            this.utils.showErrorMessage('Email address could not be found.');
            break;
          case 'EMAIL_EXISTS':
            this.utils.showErrorMessage('This email already exists!');
            break;
        }
        this.utils.isLoading.next(false);
      }
    );
  };

  azureLogin($event?: MouseEvent) {
    if ($event) {
      $event.preventDefault();
    }
    window.location.href = this.utils.replacePlaceHolder(
      environment.azure.ssoUrl,
      {
        tenantId: environment.azure.tenantId,
        clientId: environment.azure.clientId,
        redirectURI: environment.azure.redirectURI,
      }
    );
  }

  setUserData = (userData: AuthResponseData) => {
    const expirationTime = new Date(
      new Date().getTime() + +userData.expiresIn * 1000
    );
    const user = new User(
      userData.localId,
      userData.email,
      userData.idToken,
      +userData.expiresIn
    );
    this._subject.next(user);
    this.autoLogout(user.tokenDuration);
    this.storeAuthData(
      userData.localId,
      userData.idToken,
      +userData.expiresIn,
      userData.email
    );
  };

  private storeAuthData = (
    userId: string,
    token: string,
    expiredIn: number,
    email: string
  ) => {
    const data = JSON.stringify({ userId, token, expiredIn, email });
    sessionStorage.setItem('authData', data);
  };
}
