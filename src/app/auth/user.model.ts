export class User {
  constructor(
    public id: string,
    public email: string,
    private _token: string,
    private tokenExpirationIn: number
  ) {}

  get token() {
    if (!this.tokenExpirationIn || this.tokenExpirationIn < 0) {
      return null;
    }
    return this._token;
  }

  get tokenDuration() {
    if (!this.token) {
      return 0;
    }
    return this.tokenExpirationIn * 1000;
  }
}
