import {
  Component,
  ElementRef,
  inject,
  OnDestroy,
  OnInit,
  Renderer2,
} from '@angular/core';
import { Subscription } from 'rxjs';
import { AuthService } from './auth/auth.service';
import { ActivatedRoute, Router } from '@angular/router';
import { UtilsService } from './services/utils.service';
import { AzureSSOResponse } from './shared/models';
import { ToastModule } from 'primeng/toast';
import { MenuBarComponent } from './general-components/menu-bar/menu-bar.component';
import { NgxSpinnerModule } from 'ngx-spinner';
import { MessageService } from 'primeng/api';
import { SocketService } from './services/socket.service';
import { EventService } from './services/event.service';
import { FirebaseService } from './services/firebase.service';
import { SystemStore } from './signals/store/system.store';

@Component({
  selector: 'app-root',
  standalone: true,
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  imports: [ToastModule, NgxSpinnerModule, MenuBarComponent],
  providers: [MessageService],
})
export class AppComponent implements OnInit, OnDestroy {
  title = 'UsefulTools';
  private authSub!: Subscription;
  private previousAuthState = false;
  private utils = inject(UtilsService);
  private authService = inject(AuthService);
  private router = inject(Router);
  private activatedRoute = inject(ActivatedRoute);
  private elementRef = inject(ElementRef);
  private renderer = inject(Renderer2);
  private eventsService = inject(EventService);
  private observer = new MutationObserver(() => {
    this.utils.cssClassParsing(this.elementRef, this.renderer);
  });
  private socketService = inject(SocketService);
  private firebaseSerive = inject(FirebaseService);
  private systemStore = inject(SystemStore);

  ngOnDestroy(): void {
    if (this.authSub) {
      this.authSub.unsubscribe();
    }
  }

  ngOnInit(): void {
    this.firebaseSerive.listenForMessages();
    this.socketService.on<boolean>('pushSubscription').subscribe({
      next: (isSubscribed: boolean) => {
        this.systemStore.checkForPushSubscription(isSubscribed);
      },
    });
    this.observer.observe(document.body, {
      childList: true, // Detects addition/removal of child elements
      subtree: true, // Observes changes to all descendant nodes
    });
    if (!this.utils.isProduction()) {
      localStorage.setItem('mode', 'remote');
    }
    this.authService.validateToken();
    this.authSub = this.authService.isAuthenticated.subscribe(
      (isAuthenticated) => {
        if (!isAuthenticated && this.previousAuthState !== isAuthenticated) {
          this.router.navigateByUrl('/auth');
        }
        this.previousAuthState = isAuthenticated;
      }
    );
    this.activatedRoute.fragment.subscribe({
      next: (fragment) => {
        if (fragment) {
          const token =
            this.utils.formatUrlFragment<AzureSSOResponse>(fragment);
          this.authService.isAzureUser.next(true);
          this.authService.azureToken.next(token.access_token);
          setTimeout(() => {
            sessionStorage.removeItem('azureToken');
            this.authService.tokenRefresh.next(true);
          }, token.expires_in * 1000);
          sessionStorage.setItem('azureToken', JSON.stringify(token));
          this.authService.authenticate(
            'theanh2906@gmail.com',
            'BenNa1402*',
            token.expires_in
          );
        }
      },
    });

    this.authService.tokenRefresh.subscribe({
      next: () => {
        this.authService.azureLogin();
      },
    });
  }
}
