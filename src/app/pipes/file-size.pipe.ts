import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'fileSize',
  standalone: true,
})
export class FileSizePipe implements PipeTransform {
  transform(value: any): any {
    if (value === 0) {
      return '0 Bytes';
    }

    const units = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB'];
    let i = 0;
    while (value >= 1024) {
      value /= 1024;
      i++;
    }

    return value.toFixed(1) + ' ' + units[i];
  }
}
