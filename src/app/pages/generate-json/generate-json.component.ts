import { Component, Inject, OnInit } from '@angular/core';
import { MessageService } from 'primeng/api';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { FileService, UploadedFile } from '../../services/file.service';
import { DOCUMENT } from '@angular/common';

@Component({
  selector: 'app-generate-json',
  templateUrl: './generate-json.component.html',
  styleUrls: ['./generate-json.component.scss'],
  providers: [MessageService],
  standalone: true,
})
export class GenerateJsonComponent implements OnInit {
  inputFiles: any[] = [];
  uploadedFileLists: Observable<UploadedFile[]> | undefined;

  constructor(
    @Inject(DOCUMENT) private document: Document,
    private messageService: MessageService,
    private http: HttpClient,
    private fileService: FileService
  ) {}

  ngOnInit(): void {
    this.getUploadedFile();
  }

  generateJson = () => {
    // const reader = new FileReader();
    // const data = reader.readAsArrayBuffer(this.inputFiles[0].file);
    this.messageService.add({
      severity: 'info',
      summary: 'Successful',
      detail: '',
    });
    console.log(this.inputFiles[0]);
  };

  chooseFile = (e: any) => {
    for (const file of e.files) {
      this.inputFiles.push(file);
    }
  };

  handleUpload = () => {
    // @ts-ignore
    this.document.defaultView.location.reload();
  };
  getUploadedFile = () => {
    this.uploadedFileLists = this.fileService.getAllUploadedFiles();
  };
}
