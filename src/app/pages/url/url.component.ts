import { Component, OnInit } from '@angular/core';
import { MatInputModule } from '@angular/material/input';
import { FormsModule } from '@angular/forms';
import { MatRadioModule } from '@angular/material/radio';
import { MatButtonModule } from '@angular/material/button';
import { ClipboardModule } from '@angular/cdk/clipboard';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-url',
  templateUrl: './url.component.html',
  styleUrls: ['./url.component.scss'],
  standalone: true,
  imports: [
    MatInputModule,
    FormsModule,
    MatRadioModule,
    MatButtonModule,
    ClipboardModule,
    CommonModule,
  ],
})
export class UrlComponent implements OnInit {
  choice: 'keep-link' | 'run-multi-url' = 'keep-link';
  result = '';
  input = '';

  constructor() {}

  ngOnInit(): void {
    console.log();
  }

  keepLink() {
    this.result = '';
    const value = this.input.replace(/[\r\n]{2,}/g, '\n').trim();
    const words = value.replace(/\n/g, ' ').split(' ');
    words.forEach((item) => {
      if (item.includes('https://') || item.includes('http://')) {
        item = item.trim();
        this.result = this.result + item.trim() + '\n';
      }
    });
  }

  clear() {
    this.result = '';
    this.input = '';
  }

  runMultiURL() {
    const url = this.input.split('\n');
    for (let i = 0; i <= url.length; i++) {
      window.open(url[i]);
    }
  }
}
