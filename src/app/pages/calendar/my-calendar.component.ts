import { Component, Inject } from '@angular/core';
import {
  MAT_DIALOG_DATA,
  MatDialogModule,
  MatDialogRef,
} from '@angular/material/dialog';
import { BaseComponent } from '../../base.component';
import { FullCalendarModule } from '@fullcalendar/angular';
import { MatInputModule } from '@angular/material/input';
import { FormsModule } from '@angular/forms';
import { ColorPickerModule } from 'primeng/colorpicker';
import { MatButtonModule } from '@angular/material/button';
import { AsyncPipe } from '@angular/common';
import { EventsTableComponent } from './events-table/events-table.component';
import { DatePickerComponent } from './date-picker/date-picker.component';

export interface DialogData {
  title: string;
  backgroundColor: string;
}

export interface EventsTableElement {
  id: string;
  title: string;
  start: string;
  end: string;
  allDay: boolean;
}

@Component({
  selector: 'app-calendar',
  templateUrl: './my-calendar.component.html',
  styleUrls: ['./my-calendar.component.scss'],
  standalone: true,
  imports: [
    FullCalendarModule,
    AsyncPipe,
    EventsTableComponent,
    DatePickerComponent,
  ],
})
export class MyCalendarComponent extends BaseComponent {}

@Component({
  selector: 'app-events-dialog',
  templateUrl: 'events-dialog.component.html',
  styleUrls: ['./my-calendar.component.scss'],
  standalone: true,
  imports: [
    MatDialogModule,
    MatInputModule,
    FormsModule,
    ColorPickerModule,
    MatButtonModule,
  ],
})
export class EventsDialogComponent {
  constructor(
    public dialogRef: MatDialogRef<EventsDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData
  ) {}

  onNoClick = () => {
    this.dialogRef.close();
  };
}
