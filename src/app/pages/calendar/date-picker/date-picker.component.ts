import {
  Component,
  effect,
  ElementRef,
  inject,
  OnInit,
  ViewChild,
  WritableSignal,
} from '@angular/core';
import {
  FullCalendarComponent,
  FullCalendarModule,
} from '@fullcalendar/angular';
import { AsyncPipe } from '@angular/common';
import { EventsTableComponent } from '../events-table/events-table.component';
import { BaseComponent } from '../../../base.component';
import { DateSelectArg, EventInput } from '@fullcalendar/common';
import { CalendarOptions, EventChangeArg } from '@fullcalendar/core';
import { MatDialog } from '@angular/material/dialog';
import { EventStore } from '../../../signals/store/events.store';
import { EventService } from '../../../services/event.service';
import dayGridPlugin from '@fullcalendar/daygrid';
import timeGridPlugin from '@fullcalendar/timegrid';
import interactionPlugin from '@fullcalendar/interaction';
import { ActionType } from '../../../shared/models';
import { EventsDialogComponent } from '../my-calendar.component';
import { getState } from '@ngrx/signals';

@Component({
  selector: 'app-date-picker',
  templateUrl: 'date-picker.component.html',
  styleUrls: ['../my-calendar.component.scss'],
  standalone: true,
  imports: [FullCalendarModule, AsyncPipe, EventsTableComponent],
})
export class DatePickerComponent extends BaseComponent implements OnInit {
  events: any[] = [];
  backgroundColor = '';
  selectedEvent: EventInput = [];
  title = '';
  options!: CalendarOptions;
  @ViewChild('calendar') calender!: FullCalendarComponent;
  @ViewChild('scrollTarget') scrollTarget!: ElementRef;
  public dialog = inject(MatDialog);
  public eventsStore = inject(EventStore);
  private eventService = inject(EventService);

  constructor() {
    super();
    effect(() => {
      if (this.eventsStore.success()) {
        this.handleActions();
      }
    });
    effect(() => {
      if (this.eventsStore.timestamp()) {
        console.log(this.scrollTarget);
        this.navigateEvent(getState(this.eventsStore).dateToNavigate);
        this.utils.scrollToElement(this.scrollTarget);
      }
    });
  }

  handleSelect = (arg: DateSelectArg) => {
    const dialogRef = this.dialog.open(EventsDialogComponent, {
      width: '400px',
      maxWidth: '85vw',
      height: '450px',
      data: { title: this.title, backgroundColor: this.backgroundColor },
    });
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.selectedEvent = {
          title: result.title,
          start: arg.startStr,
          end: arg.endStr,
          allDay: true,
        };
        this.eventsStore.addEvent(this.selectedEvent);
      }
    });
  };
  handleAddEvent = () => {
    const event = {
      title: 'Đèn đỏ',
      start: this.utils.getDateStringFromNow(),
      end: this.utils.getDateStringFromNow(1),
      allDay: true,
    };
    this.eventsStore.addEvent(event);
  };

  ngOnInit(): void {
    this.options = {
      initialView: 'dayGridMonth',
      footerToolbar: {
        center: this.showCalendarButtons(),
      },
      headerToolbar: {
        right: 'prev,next,today',
      },
      customButtons: {
        addNewEventButton: {
          text: 'Đèn đỏ',
          click: () => {
            this.handleAddEvent();
          },
        },
        syncCalendar: {
          text: 'Sync Calendar',
          click: () => {
            this.handleSyncCalendar();
          },
        },
      },
      // @ts-ignore
      plugins: [dayGridPlugin, timeGridPlugin, interactionPlugin],
      editable: true,
      weekends: true,
      droppable: true,
      selectable: true,
      // @ts-ignore
      select: this.handleSelect.bind(this),
      eventChange: this.handleDrop.bind(this),
      longPressDelay: 200,
      locale: 'en',
      aspectRatio: 1,
    };
  }

  handleDrop(arg: EventChangeArg) {
    this.eventsStore.editEvent(this.utils.mapToEventDto(arg.event));
  }

  showCalendarButtons() {
    return this.utils.isCloud() ? 'addNewEventButton' : 'syncCalendar';
  }

  private handleSyncCalendar() {}

  private navigateEvent(timestamp: number) {
    this.calender.getApi().gotoDate(timestamp);
  }

  private handleActions() {
    switch (this.eventsStore.action()) {
      case ActionType.FETCH:
        console.log(this.eventsStore.events());
        this.data = this.eventsStore.events as WritableSignal<any>;
        break;
      default:
        break;
    }
  }
}
