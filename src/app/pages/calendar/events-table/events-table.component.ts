import {
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  effect,
  inject,
  OnInit,
  signal,
  ViewChild,
  WritableSignal,
} from '@angular/core';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatTableModule } from '@angular/material/table';
import { MatSortModule } from '@angular/material/sort';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { CommonModule } from '@angular/common';
import { MatPaginatorModule } from '@angular/material/paginator';
import { FormsModule } from '@angular/forms';
import { Table, TableModule } from 'primeng/table';
import { BaseComponent } from '../../../base.component';
import { SelectionModel } from '@angular/cdk/collections';
import { EventStore } from '../../../signals/store/events.store';
import { EventService } from '../../../services/event.service';
import { ActionType } from '../../../shared/models';
import { EventsTableElement } from '../my-calendar.component';
import { ButtonModule } from 'primeng/button';
import { IconFieldModule } from 'primeng/iconfield';
import { InputIconModule } from 'primeng/inputicon';
import { InputTextModule } from 'primeng/inputtext';
import { Inplace, InplaceModule } from 'primeng/inplace';
import { EventInput } from '@fullcalendar/common';
import { MobileDirective } from '../../../directives/mobile.directive';
import { AutoscaleDirective } from '../../../directives/autoscale.directive';

@Component({
  selector: 'app-events-table',
  templateUrl: 'events-table.component.html',
  styleUrls: ['../my-calendar.component.scss'],
  standalone: true,
  imports: [
    MatInputModule,
    MatButtonModule,
    MatTableModule,
    MatSortModule,
    MatCheckboxModule,
    CommonModule,
    MatPaginatorModule,
    FormsModule,
    TableModule,
    ButtonModule,
    IconFieldModule,
    InputIconModule,
    InputTextModule,
    InplaceModule,
    MobileDirective,
    AutoscaleDirective,
  ],
})
export class EventsTableComponent
  extends BaseComponent
  implements OnInit, AfterViewInit
{
  @ViewChild('eventsTable', { static: true }) eventsTable!: Table;
  selection = new SelectionModel<EventsTableElement>(true, []);
  dataSource: any;
  public eventsStore = inject(EventStore);
  keyword = signal('');
  selectedEvents: any[] = [];
  cdr = inject(ChangeDetectorRef);
  private eventService = inject(EventService);

  constructor() {
    super();

    effect(() => {
      this.selectedEvents = [];
      if (this.eventsStore.success()) {
        this.handleActions();
        this.cdr.detectChanges();
      }
    });
    effect(() => {
      if (this.keyword()) {
        this.eventsTable.filterGlobal(this.keyword(), 'contains');
      }
    });
  }

  ngOnInit(): void {
    // this.eventsStore.getEvents();
  }

  ngAfterViewInit() {}

  masterToggle = () => {
    if (this.isAllSelected()) {
      this.selection.clear();
      return;
    }
    this.selection.select(...this.dataSource.data);
  };

  isAllSelected = () => {
    const numSelected = this.selection.selected.length;
    const numRow = this.dataSource?.data?.length;
    return numSelected === numRow;
  };

  checkboxLabel = (row?: EventsTableElement) => {
    if (!row) {
      return `${this.isAllSelected() ? 'deselect' : 'select'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'}`;
  };

  handleDeleteEvents = () => {
    const idList = this.selection.selected.map((value) => value.id);
    this.eventsStore.deleteEvents(idList);
  };

  refresh() {
    this.eventsStore.getEvents();
  }

  filterTable($event: Event) {
    this.keyword.set(($event.target as HTMLInputElement).value);
  }

  deleteEvents() {
    if (this.selectedEvents.length > 0) {
      this.eventsStore.deleteEvents(
        this.selectedEvents.map((event) => event.id)
      );
    }
  }

  onSaveInplace(oldEvent: any, newValue: string, inplace: Inplace) {
    if (oldEvent.title !== newValue) {
      oldEvent.title = newValue;
      this.eventsStore.editEvent(oldEvent);
    }
    inplace.deactivate();
  }

  navigateEvent(event: EventInput) {
    this.eventsStore.navigate('');
    setTimeout(() => {}, 100);
    this.eventsStore.navigate(event.start as string);
  }

  deleteEvent(event: EventInput) {
    this.eventsStore.deleteEvents([event.id as string]);
  }

  handleEnter(
    $event: KeyboardEvent,
    inplace: Inplace,
    value: { oldEvent: string; newValue: string }
  ) {
    const { oldEvent, newValue } = value;
    if ($event.key === 'Enter') {
      this.onSaveInplace(oldEvent, newValue, inplace);
    }
  }

  private handleActions() {
    switch (this.eventsStore.action()) {
      case ActionType.FETCH:
        this.data = this.eventsStore.events as WritableSignal<any>;
        break;
      default:
        break;
    }
  }
}
