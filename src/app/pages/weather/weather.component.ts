import { Component, effect, inject, OnInit, signal } from '@angular/core';
import { WeatherService } from '../../services/weather.service';
import { CONSTANTS } from '../../shared/constants';
import { FormControl, FormGroup, ReactiveFormsModule } from '@angular/forms';
import { Observable, startWith } from 'rxjs';
import { map } from 'rxjs/operators';
import { WeatherInfo } from '../../../commons/interfaces';
import { CommonModule } from '@angular/common';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { BaseComponent } from '../../base.component';

@Component({
  selector: 'app-weather',
  templateUrl: './weather.component.html',
  styleUrls: ['./weather.component.scss'],
  standalone: true,
  imports: [CommonModule, MatInputModule, ReactiveFormsModule, MatButtonModule],
})
export class WeatherComponent extends BaseComponent implements OnInit {
  selectedPlace = '';
  allCountries = CONSTANTS.ALL_COUNTRY;
  weatherInfo!: WeatherInfo;
  weatherForm = new FormGroup({});
  placeControl = new FormControl();
  filteredOptions!: Observable<string[]>;
  weatherService = inject(WeatherService);
  latitude = signal(0);
  longitude = signal(0);

  constructor() {
    super();
    effect(() => {
      if (this.latitude() && this.longitude()) {
        this.forecastWithCoords(this.latitude(), this.longitude());
      }
    });
  }

  ngOnInit(): void {
    this.filteredOptions = this.placeControl.valueChanges.pipe(
      startWith(''),
      map((value) => this._filter(value || ''))
    );
  }

  forecast() {
    this.weatherService.buildQuery(this.placeControl.value);
    this.weatherService.forecast().subscribe({
      next: (res) => {
        this.weatherInfo = res;
      },
    });
  }

  forecastWithCoords(latitude: number, longitude: number) {
    this.weatherService.buildCoordsQuery(latitude, longitude);
    this.weatherService.forecast().subscribe({
      next: (res) => {
        this.weatherInfo = res;
      },
    });
  }

  getCurrentWeather() {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition((position) => {
        // this.forecastWithCoords(
        //   position.coords.latitude,
        //   position.coords.longitude
        // );
        this.latitude.set(position.coords.latitude);
        this.longitude.set(position.coords.longitude);
      });
    }
  }

  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();

    return this.allCountries.filter((option) =>
      option.toLowerCase().includes(filterValue)
    );
  }
}
