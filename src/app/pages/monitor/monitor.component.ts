import { Component, inject, OnInit, ViewChild } from '@angular/core';
import { MeterGroupModule } from 'primeng/metergroup';
import { ChartModule, UIChart } from 'primeng/chart';
import { BaseComponent } from '../../base.component';
import { SocketService } from '../../services/socket.service';
import { TableModule } from 'primeng/table';
import { UpperCasePipe } from '@angular/common';

@Component({
  selector: 'app-monitor',
  standalone: true,
  imports: [MeterGroupModule, ChartModule, TableModule, UpperCasePipe],
  templateUrl: './monitor.component.html',
  styleUrl: './monitor.component.scss',
})
export class MonitorComponent extends BaseComponent implements OnInit {
  socketService = inject(SocketService);
  ramData: any;
  options: any;
  systemInfo: any[] = [];
  @ViewChild('ramChart') ramChart!: UIChart;

  ngOnInit() {
    this.ramData = {
      labels: ['Used', 'Free'],
      datasets: [
        {
          data: [540, 325],
          backgroundColor: ['red', 'green'],
        },
      ],
    };
    this.options = {
      plugins: {
        legend: {
          labels: {
            usePointStyle: true,
            color: 'black',
          },
        },
      },
      responsive: true,
      maintainAspectRatio: false,
    };
    this.socketService.on('monitor').subscribe((res: any) => {
      const data = JSON.parse(res);
      this.systemInfo = this.utils.convertToKeyValueArray(data.system_info);
      this.updateRAMData(data);
    });
  }

  updateRAMData(payload: any) {
    const { used_memory, free_memory, raw_data } = payload;
    this.ramData.datasets[0].data = [
      raw_data.used_memory,
      raw_data.free_memory,
    ];
    this.ramData.labels = [`Used : ${used_memory}`, `Free: ${free_memory}`];
    if (this.ramChart.chart) {
      this.ramChart.chart.update();
    }
  }
}
