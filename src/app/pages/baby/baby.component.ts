import { Component, ViewChild } from '@angular/core';
import { Calendar, CalendarModule } from 'primeng/calendar';
import { FormsModule } from '@angular/forms';

@Component({
  selector: 'app-baby',
  templateUrl: './baby.component.html',
  styleUrls: ['./baby.component.scss'],
  standalone: true,
  imports: [CalendarModule, FormsModule],
})
export class BabyComponent {
  @ViewChild('time') timePicker!: Calendar;
  date: any;
  time: any;

  onSelectDate($event: Date) {
    alert($event);
  }
}
