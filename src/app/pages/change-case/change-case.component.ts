import {
  Component,
  computed,
  effect,
  inject,
  OnInit,
  signal,
} from '@angular/core';
import { Observable } from 'rxjs';
import { BaseComponent } from '../../base.component';
import { StringUtils } from '../../helpers/string-utils';
import { SignalsService } from '../../services/signals.service';
import { MatInputModule } from '@angular/material/input';
import { FormsModule } from '@angular/forms';
import { MatRadioModule } from '@angular/material/radio';
import { MatButtonModule } from '@angular/material/button';
import { ClipboardModule } from '@angular/cdk/clipboard';

@Component({
  selector: 'app-change-case',
  templateUrl: './change-case.component.html',
  styleUrls: ['./change-case.component.scss'],
  standalone: true,
  imports: [
    MatInputModule,
    FormsModule,
    MatRadioModule,
    MatButtonModule,
    ClipboardModule,
  ],
})
export class ChangeCaseComponent extends BaseComponent implements OnInit {
  choice: 'uppercase' | 'lowercase' | 'title' | 'camel' | 'kebab' | 'minify' =
    'uppercase';
  text: string | any;
  time = 0;
  text$!: Observable<string>;
  count$ = signal(0);
  selection = signal('');
  doubleCount$ = computed(() => this.count$() * 2);
  signalsService = inject(SignalsService);

  constructor() {
    super();
    effect(() => {
      this.text = StringUtils.transform(
        this.text,
        this.signalsService.changeTextChoice()
      );
    });
  }

  increase() {
    this.count$.update((value) => value + 1);
  }

  ngOnInit(): void {
    this.store.subscribe((state) => {
      if (state.changeCase.text) {
        this.text = state.changeCase.text;
      }
    });
  }

  handleChangeCasesClick = () => {
    this.increase();
    this.signalsService.changeTextChoice.set(this.choice);
    // this.store.dispatch(changeCase({ text: this.text, case: this.choice }));
  };
}
