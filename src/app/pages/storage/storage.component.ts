import {
  Component,
  effect,
  inject,
  OnInit,
  ViewChild,
  WritableSignal,
} from '@angular/core';
import { StorageService } from '../../services/storage.service';
import { FileUpload, FileUploadModule } from 'primeng/fileupload';
import { TreeNode } from '../../services/utils.service';
import { BaseComponent } from '../../base.component';
import { Table, TableModule } from 'primeng/table';
import { AsyncPipe, CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { FileSizePipe } from '../../pipes/file-size.pipe';
import { FilesStore } from '../../signals/store/files.store';
import { ActionType } from '../../shared/models';
import { InputTextModule } from 'primeng/inputtext';
import { TooltipModule } from 'primeng/tooltip';
import { SocketService } from 'src/app/services/socket.service';

export interface FileModel {
  name: string;
  size: number;
  type: string;
  path: string;
  children: FileModel[];
}

@Component({
  selector: 'app-storage',
  templateUrl: './storage.component.html',
  styleUrls: ['./storage.component.scss'],
  standalone: true,
  imports: [
    FileUploadModule,
    TableModule,
    AsyncPipe,
    CommonModule,
    FormsModule,
    FileSizePipe,
    InputTextModule,
    TooltipModule,
  ],
})
export class StorageComponent extends BaseComponent implements OnInit {
  isFshareLogged = false;
  count: any;
  files: TreeNode[] = [];
  errorMessage = '';
  isUploading = false;
  @ViewChild('fileTree') fileTree!: HTMLElement;
  @ViewChild('fileUpload') fileUpload!: FileUpload;
  @ViewChild('fileTable', { static: true }) fileTable!: Table;
  public storageService = inject(StorageService);
  socketService = inject(SocketService);
  selectedFiles: any[] = [];
  keyword = '';
  protected filesStore = inject(FilesStore);

  constructor() {
    super();
    effect(() => {
      if (this.filesStore.files()) {
        this.data = this.filesStore.files as WritableSignal<any>;
      }
    });
    effect(() => {
      if (this.filesStore.action() === ActionType.ADD) {
        this.fileUpload.disabled = false;
        this.fileUpload.clear();
      }
      if (this.filesStore.action() === ActionType.REMOVE) {
        this.selectedFiles = [];
      }
    });
  }

  ngOnInit(): void {
    this.filesStore.getAllFiles();
    this.socketService.on('data-update').subscribe({
      next: () => {
        this.filesStore.getAllFiles();
      },
    });
  }

  uploadFile(event: any) {
    const formData = new FormData();
    event.files.forEach((each: any) => {
      formData.append('files', each);
    });
    this.fileUpload.disabled = true;
    this.filesStore.uploadFiles(formData);
  }

  filterTable() {
    this.fileTable.filterGlobal(this.keyword, 'contains');
  }

  deleteFiles(paths: string | string[]) {
    if (paths instanceof Array) {
      this.filesStore.deleteFiles(paths);
    } else {
      this.filesStore.deleteFiles([paths]);
    }
  }

  refresh() {
    this.filesStore.getAllFiles();
  }

  mapToFileName = (files: any[]) => files.map((each) => each.name);
}
