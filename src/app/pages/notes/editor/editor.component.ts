import {
  ChangeDetectionStrategy,
  Component,
  effect,
  inject,
  Input,
  OnInit,
} from '@angular/core';
import { Note, NotesService } from '../../../services/notes.service';
import { BaseComponent } from '../../../base.component';
import { syncFirebase } from '../../../actions/app.action';
import { DialogService, DynamicDialogRef } from 'primeng/dynamicdialog';
import { AddCategoryComponent } from '../../../general-components/dialogs/add-category/add-category.component';
import { MatInputModule } from '@angular/material/input';
import { EditorModule } from 'primeng/editor';
import { FormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { CommonModule } from '@angular/common';
import { NotesStore } from '../../../signals/store/notes.store';
import { ActionType } from '../../../shared/models';

@Component({
  selector: 'app-editor',
  templateUrl: './editor.component.html',
  styleUrls: ['./editor.component.scss'],
  standalone: true,
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [
    MatInputModule,
    EditorModule,
    FormsModule,
    MatButtonModule,
    CommonModule,
  ],
})
export class EditorComponent extends BaseComponent implements OnInit {
  @Input() editedNote!: Note;
  contentInputText = '';
  titleInputText = '';
  addingCategories: any[] = [];
  @Input() isEdit = false;
  addCatDialogOpen = false;
  notesService = inject(NotesService);
  dialogRef = inject(DynamicDialogRef);
  dialogService = inject(DialogService);
  noteStore = inject(NotesStore);

  constructor() {
    super();
    effect(() => {
      if (this.noteStore.noteToEdit()) {
        this.editedNote = this.noteStore.noteToEdit();
      }
      this.contentInputText = this.editedNote ? this.editedNote.content : '';
      this.titleInputText = this.editedNote ? this.editedNote.title : '';
    });
    effect(() => {
      if ([ActionType.ADD, ActionType.EDIT].includes(this.noteStore.action())) {
        this.dialogRef.close();
      }
    });
  }

  ngOnInit(): void {}

  onCreateNote(title: string, content: string) {
    if (!title || !content) {
      return;
    }
    const note: Note = {
      id: '',
      title,
      content,
      createdDate: new Date().getTime(),
    };
    this.noteStore.addNote(note);
  }

  onClear() {
    this.titleInputText = '';
    this.contentInputText = '';
  }

  onEdit() {
    const id = this.editedNote.id;
    const editNote: Note = {
      id: this.editedNote.id,
      title: this.titleInputText,
      content: this.contentInputText,
      createdDate: this.editedNote.createdDate,
    };
    this.notesService.editNote(editNote).subscribe();
  }

  openAddCatDialog() {
    this.dialogRef = this.dialogService.open(AddCategoryComponent, {
      closeOnEscape: true,
      header: 'Add category',
    });
  }

  syncNotes() {
    this.store.dispatch(syncFirebase());
  }

  saveCategory($event: any) {}
}
