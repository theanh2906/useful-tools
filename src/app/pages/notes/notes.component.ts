import {
  Component,
  effect,
  inject,
  OnInit,
  WritableSignal,
} from '@angular/core';
import { Note } from '../../services/notes.service';
import { BaseComponent } from '../../base.component';
import { AsyncPipe, CommonModule } from '@angular/common';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { EditorComponent } from './editor/editor.component';
import { NotesStore } from '../../signals/store/notes.store';
import { ActionType, Mode } from '../../shared/models';
import { CdkDrag, CdkDropList } from '@angular/cdk/drag-drop';
import { MobileDirective } from '../../directives/mobile.directive';
import { DialogService } from 'primeng/dynamicdialog';
import { Button } from 'primeng/button';
import { AddCategoryComponent } from '../../general-components/dialogs/add-category/add-category.component';

@Component({
  selector: 'app-notes',
  templateUrl: './notes.component.html',
  styleUrls: ['./notes.component.scss'],
  standalone: true,
  providers: [DialogService],
  imports: [
    AsyncPipe,
    MatTooltipModule,
    MatIconModule,
    CommonModule,
    MatButtonModule,
    EditorComponent,
    CdkDropList,
    CdkDrag,
    MobileDirective,
    Button,
  ],
})
export class NotesComponent extends BaseComponent implements OnInit {
  editedNote!: Note;
  notesStore = inject(NotesStore);
  dialogService = inject(DialogService);
  dialogRef: any;
  protected readonly Mode = Mode;
  protected readonly ActionType = ActionType;

  constructor() {
    super();
    effect(
      () => {
        this.handleActions();
      },
      {
        allowSignalWrites: true,
      }
    );
    effect(() => {
      if (this.notesStore.noteToEdit()) {
        this.dialogRef = this.dialogService.open(EditorComponent, {
          closeOnEscape: true,
          header: `Edit note`,
        });
      }
    });
  }

  ngOnInit(): void {
    this.notesStore.getNotes();
  }

  deleteNote(id: string) {
    this.notesStore.deleteNote(id);
  }

  onSwitchMode(mode: ActionType, note?: any) {
    switch (mode) {
      case ActionType.ADD:
        this.dialogService.open(EditorComponent, {
          closeOnEscape: true,
          header: 'Add note',
        });
        break;
      case ActionType.EDIT:
        if (note) {
          this.notesStore.editNote(note);
        }
        break;
      default:
        break;
    }
  }

  handleActions() {
    switch (this.notesStore.action()) {
      case ActionType.FETCH:
        this.data = this.notesStore.notes as WritableSignal<any>;
        break;
      case ActionType.REMOVE:
        this.notesStore.getNotes();
        break;
      case ActionType.ADD:
        this.notesStore.getNotes();
        break;
      case ActionType.EDIT:
        this.notesStore.getNotes();
        break;
      default:
        break;
    }
  }

  openCategoryDialog() {
    this.dialogService.open(AddCategoryComponent, {
      closeOnEscape: true,
      header: 'Add category',
      height: '400px',
    });
  }
}
