import { Component, inject, OnInit } from '@angular/core';
import { BarcodeService } from '../../services/barcode.service';
import { UtilsService } from '../../services/utils.service';
import { MatInputModule } from '@angular/material/input';
import { FormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-qr-generator',
  templateUrl: './qr-generator.component.html',
  styleUrls: ['./qr-generator.component.scss'],
  standalone: true,
  imports: [MatInputModule, FormsModule, MatButtonModule, CommonModule],
})
export class QrGeneratorComponent implements OnInit {
  text = '';
  qrCode = '';
  private barcodeService = inject(BarcodeService);
  private utils = inject(UtilsService);

  constructor() {}

  ngOnInit(): void {}

  generateQrCode() {
    this.barcodeService.generateQrCode(this.text).subscribe({
      next: (res) => {
        this.qrCode = res.data;
      },
    });
  }

  downloadQrCode() {
    window.open(`${this.utils.getApiUrl()}/qr/download` + '?text=' + this.text);
  }
}
