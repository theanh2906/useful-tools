import { Component, inject, OnInit, ViewChild } from '@angular/core';
import {
  FileUpload,
  FileUploadHandlerEvent,
  FileUploadModule,
} from 'primeng/fileupload';
import * as JSZip from 'jszip';
import { BaseComponent } from '../../base.component';
import { StorageService } from '../../services/storage.service';

@Component({
  selector: 'app-zip',
  templateUrl: './zip.component.html',
  styleUrls: ['./zip.component.scss'],
  standalone: true,
  imports: [FileUploadModule],
})
export class ZipComponent extends BaseComponent implements OnInit {
  @ViewChild('fileUpload') fileUpload!: FileUpload;
  @ViewChild('downloadLink') downloadLink!: HTMLAnchorElement;
  zip: any;
  storageService = inject(StorageService);

  uploadFile($event: FileUploadHandlerEvent) {
    this.utils.isLoading.next(true);
    const formData = new FormData();
    $event.files.forEach((each: any) => {
      formData.append('files', each);
    });
    this.storageService.createZip(formData).subscribe({
      next: () => {
        this.storageService.downloadFile('files.zip');
        this.utils.isLoading.next(false);
      },
    });
  }

  ngOnInit(): void {
    this.zip = new JSZip();
  }
}
