import { Component, inject, OnInit } from '@angular/core';
import { AuthService } from '../../auth/auth.service';
import {
  FormControl,
  FormsModule,
  ReactiveFormsModule,
  Validators,
} from '@angular/forms';
import { CachesService } from '../../services/caches.service';
import { ActivatedRoute, Router } from '@angular/router';
import { MessageService } from 'primeng/api';
import { GoogleService } from '../../services/google.service';
import { UtilsService } from '../../services/utils.service';
import { MatInputModule } from '@angular/material/input';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { CommonModule } from '@angular/common';
import StatusResponse = facebook.StatusResponse;

@Component({
  selector: 'app-authentication',
  templateUrl: './authentication.component.html',
  styleUrls: ['./authentication.component.scss'],
  standalone: true,
  imports: [
    ReactiveFormsModule,
    MatInputModule,
    MatIconModule,
    FormsModule,
    MatButtonModule,
    CommonModule,
  ],
})
export class AuthenticationComponent implements OnInit {
  email = new FormControl('', [
    Validators.required,
    Validators.pattern(/^\w+(-?\w+)*@\w+(-?\w+)*(\.\w{2,3})+$/),
  ]);
  password = new FormControl('', [Validators.required]);
  authService = inject(AuthService);
  public utils = inject(UtilsService);
  private cachesService = inject(CachesService);
  private router = inject(Router);
  private messageService = inject(MessageService);
  private google = inject(GoogleService);
  private route = inject(ActivatedRoute);

  constructor() {
    FB.init({
      appId: '1070009906931041',
      cookie: true,
      xfbml: true,
      status: true,
      version: 'v14.0',
      autoLogAppEvents: true,
    });
  }

  ngOnInit(): void {}

  onSwitchMode = () => {
    this.authService.isLogin = !this.authService.isLogin;
  };

  getEmailErrorMessage = () => {
    if (this.email.hasError('required')) {
      return 'This field is required';
    }
    return this.email.hasError('pattern') ? 'Invalid email address!' : '';
  };
  getPasswordErrorMessage = () => {
    if (this.email.hasError('required')) {
      return 'This field is required';
    }
    return '';
  };

  fbLogin($event: any) {
    $event.preventDefault();
    FB.login(
      (response: StatusResponse) => {
        console.log(response);
        FB.api('/me', (res: any) => {
          if (res.name === 'The Anh') {
            this.authService.authenticate(
              'theanh2906@gmail.com',
              'BenNa1402*',
              response.authResponse.expiresIn
            );
          }
        });
      },
      {
        scope: 'email',
      }
    );
  }

  ggLogin($event: MouseEvent) {
    $event.preventDefault();
    this.google.signIn();
  }
}
