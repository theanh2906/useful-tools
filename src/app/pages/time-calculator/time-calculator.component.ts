import { Component, OnInit } from '@angular/core';
import {
  FormControl,
  FormGroup,
  FormsModule,
  ReactiveFormsModule,
} from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { ButtonModule } from 'primeng/button';

@Component({
  selector: 'app-time-calculator',
  templateUrl: './time-calculator.component.html',
  styleUrls: ['./time-calculator.component.scss'],
  standalone: true,
  imports: [
    FormsModule,
    MatInputModule,
    MatDatepickerModule,
    ReactiveFormsModule,
    ButtonModule,
  ],
})
export class TimeCalculatorComponent implements OnInit {
  fromDate: any;
  toDate: any;
  result = '';
  dateCal = new FormGroup({
    fromDate: new FormControl(),
    toDate: new FormControl(),
    result: new FormControl(),
  });

  constructor() {}

  ngOnInit(): void {}

  onSelectDate() {
    if (this.fromDate && this.toDate) {
      const year = Math.floor(
        Math.abs(
          (this.getTime(this.fromDate) - this.getTime(this.toDate)) /
            (365 * 24 * 60 * 60 * 1000)
        )
      );
      const month =
        Math.floor(
          Math.abs(
            (this.getTime(this.fromDate) - this.getTime(this.toDate)) /
              (30 * 24 * 60 * 60 * 1000)
          )
        ) -
        year * 12;
      const day = Math.floor(
        Math.abs(
          (this.getTime(this.fromDate) - this.getTime(this.toDate)) /
            (24 * 60 * 60 * 1000)
        ) -
          (year * 12 * 30 + month * 30)
      );
      this.result = `${year} year${year > 1 ? 's' : ''} ${month} month${
        month > 1 ? 's' : ''
      } ${day} day${day > 1 ? 's' : ''}`;
    }
  }

  clear() {
    this.dateCal.reset();
  }

  private getTime(date: string) {
    return new Date(date).getTime();
  }
}
