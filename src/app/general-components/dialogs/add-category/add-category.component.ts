import { Component } from '@angular/core';
import {
  FormControl,
  FormGroup,
  ReactiveFormsModule,
  Validators,
} from '@angular/forms';
import { ButtonModule } from 'primeng/button';
import { ColorPickerModule } from 'primeng/colorpicker';
import { InputTextModule } from 'primeng/inputtext';

@Component({
  selector: 'app-add-category',
  templateUrl: './add-category.component.html',
  styleUrls: ['./add-category.component.scss'],
  standalone: true,
  imports: [
    ButtonModule,
    ColorPickerModule,
    ReactiveFormsModule,
    InputTextModule,
  ],
})
export class AddCategoryComponent {
  categoryForm = new FormGroup({
    categoryName: new FormControl('', [Validators.required]),
    categoryColor: new FormControl('', [Validators.required]),
  });
}
