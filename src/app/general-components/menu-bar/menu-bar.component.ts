import {
  ChangeDetectorRef,
  Component,
  computed,
  inject,
  OnInit,
  ViewChild,
} from '@angular/core';
import { MediaMatcher } from '@angular/cdk/layout';
import { Router, RouterModule } from '@angular/router';
import { MatSidenav, MatSidenavModule } from '@angular/material/sidenav';
import { CachesService } from '../../services/caches.service';
import {
  MatSlideToggleChange,
  MatSlideToggleModule,
} from '@angular/material/slide-toggle';
import { BaseComponent } from '../../base.component';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';
import { BadgeModule } from 'primeng/badge';
import { CommonModule } from '@angular/common';
import { MatListModule } from '@angular/material/list';
import { MatButtonModule } from '@angular/material/button';
import { FirebaseService } from '../../services/firebase.service';
import { PushNotificationService } from '../../services/push-notification.service';
import { SystemStore } from '../../signals/store/system.store';

type SideBarItem = {
  title: string;
  url: string;
  icon?: string;
  show?: boolean;
  children?: SideBarChildItem[];
};

type SideBarChildItem = {
  title: string;
  url: string;
  icon?: string;
  show?: boolean;
};

@Component({
  selector: 'app-menu-bar',
  templateUrl: './menu-bar.component.html',
  styleUrls: ['./menu-bar.component.scss'],
  standalone: true,
  imports: [
    MatToolbarModule,
    MatSidenavModule,
    MatIconModule,
    BadgeModule,
    MatSlideToggleModule,
    CommonModule,
    RouterModule,
    MatListModule,
    MatButtonModule,
  ],
})
export class MenuBarComponent extends BaseComponent implements OnInit {
  @ViewChild('sideNav') sideNav!: MatSidenav;
  @ViewChild('openButton') openButton!: HTMLElement;
  isAuthenticated = false;
  mobileQuery!: MediaQueryList;
  title = '';
  sideNavItems: SideBarItem[] = [];
  mobileQueryListener: (() => void) | undefined;
  public changeDetectorRef = inject(ChangeDetectorRef);
  noti: any[] = [];
  systemStore = inject(SystemStore);
  isPushSubscribed = computed(() => this.systemStore.pushSubscription());
  private media = inject(MediaMatcher);
  private router = inject(Router);
  private cachesService = inject(CachesService);
  private firebaseService = inject(FirebaseService);
  private pushNotificationService = inject(PushNotificationService);

  constructor() {
    super();
    this.authService.isAuthenticated.subscribe((authenticated) => {
      this.mobileQuery = this.media.matchMedia('(max-width: 600px)');
      this.mobileQueryListener = () => this.changeDetectorRef.detectChanges();
      this.mobileQuery.addEventListener(
        'change',
        () => this.mobileQueryListener
      );
      this.sideNavItems = this.utils.getMenu(authenticated);
    });
    this.store.subscribe({
      next: (res) => {
        if (res.notification) {
          this.noti = res.notification.notificationList;
        }
      },
    });
  }

  ngOnInit(): void {
    this.authService.isAuthenticated.subscribe((isAuthenticated) => {
      this.isAuthenticated = isAuthenticated;
    });
  }

  onNavigate = (url: string) => {
    this.cachesService.setPreviousUrl(url);
    if (this.mobileQuery.matches) {
      this.sideNav.close().then(() => {
        this.router.navigate([url]).catch(console.log);
      });
    } else {
      this.router.navigate([url]);
    }
  };

  onAuthenticate = () => {
    if (this.isAuthenticated) {
      this.authService.logout();
    }
    this.router.navigateByUrl('/auth');
    this.snackBar.open('You have successfully signed out!', '', {
      duration: 2000,
    });
  };

  switchMode($event: MatSlideToggleChange) {
    if ($event.checked) {
      localStorage.setItem('mode', 'remote');
      location.reload();
    } else {
      localStorage.setItem('mode', 'cloud');
      location.reload();
    }
  }

  async subscribePush() {
    const permission = await Notification.requestPermission();
    if (permission === 'granted') {
      if ('serviceWorker' in navigator) {
        const registration = await navigator.serviceWorker.register(
          '/firebase-messaging-sw.js',
          {
            scope: 'firebase-cloud-messaging-push-scope',
          }
        );

        const subscribeToken = async () => {
          const token =
            await this.pushNotificationService.getFcmToken(registration);
          this.pushNotificationService
            .sendSubscriptionToServer(token)
            .subscribe();
        };

        if (await registration.pushManager.getSubscription()) {
          await subscribeToken();
        } else {
          await this.utils.sleep(500);
          await subscribeToken();
        }
      }
    }
  }
}
