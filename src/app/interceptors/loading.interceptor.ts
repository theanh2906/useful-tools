import { inject, Injectable } from '@angular/core';
import {
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
} from '@angular/common/http';
import { finalize, Observable } from 'rxjs';
import { UtilsService } from '../services/utils.service';

@Injectable()
export class LoadingInterceptor implements HttpInterceptor {
  utils = inject(UtilsService);

  constructor() {}

  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    this.utils.isLoading.next(true);
    return next.handle(request).pipe(
      finalize(() => {
        this.utils.isLoading.next(false);
      })
    );
  }
}
