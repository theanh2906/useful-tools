import { Injectable } from '@angular/core';
import {
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { StorageService } from '../services/storage.service';

@Injectable()
export class FshareTokenInterceptor implements HttpInterceptor {
  constructor(private storageService: StorageService) {}

  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    if (this.storageService.fshareData) {
      const modifiedRequest = request.clone({
        headers: request.headers
          .set('sessionId', this.storageService.fshareData.session_id)
          .set('token', this.storageService.fshareData.token),
      });
      return next.handle(modifiedRequest);
    } else {
      return next.handle(this.modifyRequest(request));
    }
  }

  modifyRequest(request: HttpRequest<any>) {
    if (request.url.includes('https://accounts.google.com/o/oauth2/')) {
      return request.clone({
        headers: request.headers.set(
          'Cross-Origin-Opener-Policy',
          'same-origin-allow-popups'
        ),
      });
    }
    return request;
  }
}
