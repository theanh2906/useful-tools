import { createReducer, on } from '@ngrx/store';
import { notificationAction } from '../actions/app.action';

const initialState: NotificationState = {
  count: 0,
  notificationList: [],
};

export interface NotificationState {
  count: number;
  notificationList: string[];
}

export const notificationReducer = createReducer(
  initialState,
  on(notificationAction, (state, action) => {
    return {
      count: state.count + 1,
      notificationList: state.notificationList.concat(action.detail),
    };
  })
);
