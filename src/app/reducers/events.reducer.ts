import { createReducer, on } from '@ngrx/store';
import { ActionType } from '../shared/models';
import { eventsAction } from '../actions/app.action';

const initialState: EventState = {
  data: {},
};

export interface EventState {
  data?: any;
  action?: ActionType;
}

export const eventsReducer = createReducer(
  initialState,
  on(eventsAction, (state, action) => {
    // if (
    //   action.actionType === ActionType.ADD ||
    //   ActionType.REMOVE ||
    //   ActionType.EDIT
    // ) {
    //   return { refresh: true };
    // }
    // return { refresh: false };
    switch (action.actionType) {
      case ActionType.ADD:
        return { action: ActionType.ADD, data: action.data };
      case ActionType.REMOVE:
        return { action: ActionType.REMOVE, data: action.data };
      default:
        return {};
    }
  })
);
