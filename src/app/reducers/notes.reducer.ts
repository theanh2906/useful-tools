import { createReducer, on } from '@ngrx/store';
import { ActionType } from '../shared/models';
import { notesAction } from '../actions/app.action';

const initialState: NoteState = {
  refreshNotes: false,
  cancelNoteIds: '',
};

export interface NoteState {
  refreshNotes?: boolean;
  cancelNoteIds?: string;
}

export const notesReducer = createReducer(
  initialState,
  on(notesAction, (state, action) => {
    if (
      action.actionType === ActionType.ADD ||
      ActionType.REMOVE ||
      ActionType.SYNC ||
      ActionType.EDIT ||
      ActionType.FETCH
    ) {
      return { refreshNotes: true };
    }
    return { refreshNotes: false };
  })
);
