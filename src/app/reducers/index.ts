import { changeCaseReducer, TextState } from './change-case.reducer';
import { notesReducer, NoteState } from './notes.reducer';
import { eventsReducer, EventState } from './events.reducer';
import { notificationReducer, NotificationState } from './notification.reducer';
import { dataReducer, DataState } from './data.reducer';

export interface AppState {
  changeCase: TextState;
  notes: NoteState;
  events: EventState;
  notification: NotificationState;
  data: DataState;
}

export const reducers = {
  changeCase: changeCaseReducer,
  notes: notesReducer,
  events: eventsReducer,
  notification: notificationReducer,
  data: dataReducer,
};
