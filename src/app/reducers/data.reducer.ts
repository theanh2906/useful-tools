import { createReducer, on } from '@ngrx/store';
import { fetchDataSuccess, onRefresh } from '../actions/app.action';

export interface DataState {
  data: any;
  refresh: boolean;
}

const initialState: DataState = {
  data: null,
  refresh: false,
};

export const dataReducer = createReducer(
  initialState,
  on(fetchDataSuccess, (state, { data }) => ({
    data,
    refresh: true,
  })),
  on(onRefresh, (state, { refresh }) => ({
    data: null,
    refresh,
  }))
);
