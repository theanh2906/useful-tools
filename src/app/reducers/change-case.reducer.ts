import { createReducer, on } from '@ngrx/store';
import { StringUtils } from '../helpers/string-utils';
import { changeCase } from '../actions/app.action';

const initialState: TextState = {
  text: '',
  time: 0,
};

export interface TextState {
  text: string;
  time: number;
}

export const changeCaseReducer = createReducer(
  initialState,
  on(changeCase, (state, action) => ({
    text: StringUtils.transform(action.text, action.case),
    time: new Date().getTime(),
  }))
);
