import { StorageEffects } from './storage.effect';
import { NotesEffect } from './notes.effect';

export const effects = [StorageEffects, NotesEffect];
