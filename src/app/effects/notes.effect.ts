import { createEffect, ofType } from '@ngrx/effects';
import { switchMap, tap } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { BaseEffect } from './base.effect';
import {
  addNote,
  deleteNote,
  getAllNotes,
  syncFirebase,
} from '../actions/app.action';

@Injectable()
export class NotesEffect extends BaseEffect {
  getAllNotes$ = createEffect(() => {
    return this.action$.pipe(
      ofType(getAllNotes),
      switchMap((action) =>
        this.notesService
          .getAllNotes()
          .pipe(switchMap(async (data) => this.fetchDataSuccess({ data })))
      )
    );
  });

  deleteNote$ = createEffect(() => {
    return this.action$.pipe(
      ofType(deleteNote),
      switchMap((action) =>
        this.notesService
          .deleteNote(action.id)
          .pipe(tap(() => this.store.dispatch(getAllNotes())))
      )
    );
  });

  syncFirebase$ = createEffect(() => {
    return this.action$.pipe(
      ofType(syncFirebase),
      switchMap((action) =>
        this.notesService
          .syncNotes()
          .pipe(tap(() => this.store.dispatch(getAllNotes())))
      )
    );
  });

  addNote$ = createEffect(() => {
    return this.action$.pipe(
      ofType(addNote),
      switchMap((action) =>
        this.notesService
          .addNote(action.note)
          .pipe(tap(() => this.store.dispatch(getAllNotes())))
      )
    );
  });
}
