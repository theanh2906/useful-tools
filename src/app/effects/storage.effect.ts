import { Injectable } from '@angular/core';
import { createEffect, ofType } from '@ngrx/effects';
import { catchError, switchMap, tap } from 'rxjs/operators';
import { deleteFiles, getAllFiles, uploadFiles } from '../actions/app.action';
import { BaseEffect } from './base.effect';
import { of } from 'rxjs';

@Injectable()
export class StorageEffects extends BaseEffect {
  getAllFiles$ = createEffect(() => {
    return this.action$.pipe(
      ofType(getAllFiles),
      switchMap((action) =>
        this.storageService
          .getAllFiles()
          .pipe(switchMap(async (data) => this.fetchDataSuccess({ data })))
      )
    );
  });
  deleteFile$ = createEffect(() => {
    return this.action$.pipe(
      ofType(deleteFiles),
      switchMap((action) =>
        this.storageService
          .deleteFiles(action.paths)
          .pipe(switchMap(async () => getAllFiles()))
      )
    );
  });
  uploadFiles$ = createEffect(() => {
    return this.action$.pipe(
      ofType(uploadFiles),
      switchMap((action) =>
        this.storageService.upload(action.formData).pipe(
          tap(() => {
            this.messageService.add({
              key: 'fileUpload',
              severity: 'success',
              summary: 'File Uploaded',
              detail: 'Successfully uploaded file',
            });
          }),
          catchError((err) => {
            this.messageService.add({
              key: 'fileUpload',
              severity: 'error',
              summary: 'File Upload',
              detail: 'Failed to upload file',
            });
            return of(err);
          })
        )
      )
    );
  });
}
