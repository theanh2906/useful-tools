import { inject } from '@angular/core';
import { Actions } from '@ngrx/effects';
import { NotesService } from '../services/notes.service';
import { StorageService } from '../services/storage.service';
import { MessageService } from 'primeng/api';
import { fetchDataSuccess } from '../actions/app.action';
import { AppState } from '../reducers';
import { Store } from '@ngrx/store';

export class BaseEffect {
  protected action$ = inject(Actions);
  protected notesService = inject(NotesService);
  protected storageService = inject(StorageService);
  protected fetchDataSuccess = fetchDataSuccess;
  protected messageService = inject(MessageService);
  protected store = inject(Store<AppState>);
}
