import { FileFolder } from './storage.service';
import { MediaMatcher } from '@angular/cdk/layout';
import { ElementRef, inject, Injectable, Renderer2 } from '@angular/core';
import { environment } from '../../environments/environment';
import { MessageService } from 'primeng/api';
import { NgxSpinnerService } from 'ngx-spinner';
import { BehaviorSubject } from 'rxjs';
import { cssAttributeMapping } from '../shared/models';

export interface TreeNode<T = any> {
  label?: string;
  data?: T;
  icon?: string;
  expandedIcon?: any;
  collapsedIcon?: any;
  children?: TreeNode<T>[];
  leaf?: boolean;
  expanded?: boolean;
  type?: string;
  parent?: TreeNode<T>;
  partialSelected?: boolean;
  styleClass?: string;
  draggable?: boolean;
  droppable?: boolean;
  selectable?: boolean;
  key?: string;
  path?: string;
  size?: string;
  mimetype?: string;
}

@Injectable({
  providedIn: 'root',
})
export class UtilsService {
  public messageService = inject(MessageService);
  public isLoading = new BehaviorSubject<boolean>(false);
  private media = inject(MediaMatcher);
  private spinner = inject(NgxSpinnerService);

  constructor() {
    this.isLoading.subscribe({
      next: (value) => {
        value ? this.spinner.show() : this.spinner.hide();
      },
    });
  }

  public get isMobile() {
    return this.mediaMatch(600);
  }

  public mediaMatch(maxWidth: number) {
    return this.media.matchMedia(`(max-width: ${maxWidth}px)`).matches;
  }

  public createPathFromType(type: string): string {
    if (type) {
      const fileType = type.split('/')[0];
      return fileType[0].toUpperCase() + fileType.slice(1);
    } else {
      return 'Others';
    }
  }

  public mapToTreeNode(data: FileFolder): TreeNode {
    return {
      label: data.name,
      expandedIcon: data.mimetype
        ? this.getFileIcon(data)
        : 'pi pi-folder-open',
      collapsedIcon: data.mimetype ? this.getFileIcon(data) : 'pi pi-folder',
      children: data.children ? this.mapToTreeNodes(data.children) : [],
      path: data.path,
      size: data.size,
      mimetype: data.mimetype,
      draggable: true,
      droppable: true,
    };
  }

  public mapToTreeNodes(data: FileFolder[]): TreeNode[] {
    return data.map((item) => this.mapToTreeNode(item));
  }

  public getFileIcon(file: FileFolder): string {
    if (file.mimetype.includes('image')) {
      return 'pi pi-image';
    } else if (file.mimetype.includes('audio')) {
      return 'pi pi-volume-up';
    } else if (file.mimetype.includes('video')) {
      return 'pi pi-video';
    } else if (file.mimetype.includes('pdf')) {
      return 'pi pi-file-pdf';
    } else if (file.mimetype.includes('excel')) {
      return 'pi pi-file-excel';
    } else {
      return 'pi pi-file';
    }
  }

  public createFacebookScopes(scopes: string[]) {
    return scopes.join(',');
  }

  getMenu(authenticated: boolean) {
    return [
      {
        title: 'Invoice',
        url: 'invoice',
        icon: 'receipt',
        show: false,
      },
      {
        title: 'Change Cases',
        url: 'change-cases',
        icon: 'format_size',
        show: true,
      },
      {
        title: 'Calendar',
        url: 'calendar',
        icon: 'calendar_today',
        show: true,
      },
      {
        title: 'Notes',
        url: 'notes',
        icon: 'edit_note',
        show: true,
      },
      {
        title: 'URL',
        url: 'url',
        icon: 'link',
        show: false,
      },
      {
        title: 'Storage',
        url: 'storage',
        icon: 'storage',
        show: true,
      },
      {
        title: 'Time Calculator',
        url: 'time-calculator',
        icon: 'calculate',
        show: false,
      },
      {
        title: 'Weather',
        url: 'weather',
        icon: 'thermostat',
        show: true,
      },
      {
        title: 'QR Generator',
        url: 'barcode',
        icon: 'qr_code_2',
        show: !this.isCloud(),
      },
      {
        title: 'Zipping',
        url: 'zipping',
        icon: 'folder_zip',
        show: true,
      },
      {
        title: 'Baby',
        url: 'baby',
        icon: 'child_care',
        show: false,
      },
      {
        title: 'System Monitor',
        url: 'monitor',
        icon: 'legend_toggle',
        show: true,
      },
    ];
  }

  getApiUrl() {
    if (this.isCloud()) {
      return environment.server.cloudUrl;
    } else {
      return environment.server.apiUrl;
    }
  }

  getRemoteApiUrl() {
    return environment.server.apiUrl;
  }

  isCloud() {
    return environment.cloud;
  }

  getCurrentMode() {
    return this.isCloud() ? 'cloud' : 'remote';
  }

  isProduction() {
    return environment.production;
  }

  mapToEventDto(res: any) {
    return {
      id: res.id,
      title: res.title,
      start: res.startStr,
      end: res.endStr,
      allDay: res.allDay,
    };
  }

  sortAscBy = <T>(arr: T[], field: keyof T, order: 'asc' | 'desc'): T[] => {
    return arr.slice().sort((a, b) => {
      if (a[field] < b[field]) {
        return order === 'asc' ? -1 : 1;
      } else if (a[field] > b[field]) {
        return order === 'asc' ? 1 : -1;
      } else {
        return 0;
      }
    });
  };

  replacePlaceHolder(
    placeHolderText: string,
    mappedValue: { [placeHolder: string]: string }
  ) {
    Object.keys(mappedValue).forEach((each) => {
      const placeHolder = `:${each}`;
      placeHolderText = placeHolderText.replace(placeHolder, mappedValue[each]);
    });
    return placeHolderText;
  }

  formatUrlFragment<T>(fragment: string): T {
    const result: any = {};
    fragment.split('&').forEach((each) => {
      const subFragment = each.split('=');
      result[subFragment[0]] = isNaN(Number(subFragment[1]))
        ? subFragment[1]
        : +subFragment[1];
    });
    return result;
  }

  showErrorMessage = (message: string) => {
    this.messageService.add({
      key: 'appToast',
      summary: 'An error occurred!',
      detail: message,
      severity: 'error',
    });
  };

  convertTime(dateString: string) {
    const [day, month, year] = dateString.split('-').map(Number);
    return new Date(day, month - 1, year).getTime();
  }

  sortTime(a: any, b: any) {
    return this.convertTime(b.start) - this.convertTime(a.start);
  }

  scrollToElement(elemRef: ElementRef) {
    elemRef.nativeElement.scrollIntoView({
      behavior: 'smooth',
      block: 'start',
    });
  }

  getDateStringFromNow(dayDiff = 0) {
    const day = new Date().getDate() + dayDiff;
    return (
      new Date().getFullYear() +
      '-' +
      (new Date().getMonth() + 1 < 10
        ? '0' + (new Date().getMonth() + 1)
        : new Date().getMonth() + 1) +
      '-' +
      (day < 10 ? `0${day}` : day)
    );
  }

  cssClassParsing(elementRef: ElementRef, renderer: Renderer2) {
    const elements = elementRef.nativeElement.querySelectorAll('[class*=":"]');
    elements.forEach((element: HTMLElement) => {
      if (element.className.includes('mobile')) {
        return;
      }
      element.className.split(' ').forEach((className: string) => {
        if (className.includes(':')) {
          const [aviation, value] = className.split(':');
          if (cssAttributeMapping[aviation]) {
            if (typeof cssAttributeMapping[aviation] === 'string') {
              renderer.setStyle(
                element,
                cssAttributeMapping[aviation] as string,
                value
              );
            } else {
              renderer.setStyle(
                element,
                cssAttributeMapping[aviation][0],
                value
              );
              renderer.setStyle(
                element,
                cssAttributeMapping[aviation][1],
                value
              );
            }
          } else {
            renderer.setStyle(element, aviation, value);
          }
        }
      });
    });
  }

  sleep = async (ms: number): Promise<void> => {
    return new Promise((resolve) => setTimeout(resolve, ms));
  };

  convertToKeyValueArray(obj: any, parentKey: string = '') {
    let result: { property: string; value: any }[] = [];

    for (const key in obj) {
      if (obj.hasOwnProperty(key)) {
        const propName = parentKey ? `${parentKey} ${key}` : key;

        if (typeof obj[key] === 'object' && obj[key] !== null) {
          result = result.concat(
            this.convertToKeyValueArray(obj[key], propName)
          );
        } else {
          result.push({ property: propName, value: obj[key] });
        }
      }
    }

    return result;
  }
}
