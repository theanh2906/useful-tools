import { Injectable } from '@angular/core';
import { CONSTANTS } from '../shared/constants';
import { map } from 'rxjs/operators';
import { BaseService } from './base.service';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';

export interface LoginResponse {
  code: number;
  msg: string;
  token: string;
  session_id: string;
}

export interface FshareSession {
  token: string;
  session_id: string;
  expiresIn: number;
}

export interface FileFolder {
  id: string;
  linkcode: string;
  name: string;
  secure: string;
  directlink: string;
  type: string;
  path: string;
  size: string;
  downloadcount: string;
  mimetype: string;
  created: string;
  pwd: string;
  allow_follow: string;
  num_follower: string;
  children: FileFolder[];
}

@Injectable({
  providedIn: 'root',
})
export class StorageService extends BaseService {
  constructor() {
    super();
  }

  get uploadUrl() {
    return `${environment.server.apiUrl}/files`;
  }

  get isLoggedIn() {
    return this._subject.asObservable().pipe(
      map((token) => {
        if (token) {
          return !!token.token;
        } else {
          return !!localStorage.getItem(CONSTANTS.FSHARE_DATA_KEY);
        }
      })
    );
  }

  get fshareData() {
    return JSON.parse(
      localStorage.getItem(CONSTANTS.FSHARE_DATA_KEY) as string
    ) as FshareSession;
  }

  upload(formData: FormData) {
    return this.http.post<any>(this.uploadLocationUrl(), formData);
  }

  createZip(formData: FormData): Observable<Blob> {
    return this.http.post<any>(this.uploadUrl + '/zip', formData, {
      responseType: 'blob' as 'json',
    });
  }

  getAllFiles() {
    return this.http.get<any[]>(this.uploadLocationUrl());
  }

  downloadFile(path: string) {
    path = encodeURIComponent(path);
    open(`${environment.server.apiUrl}/files/${path}`, 'blank');
  }

  deleteFiles(paths: string[]) {
    return this.http.delete(this.uploadLocationUrl(), {
      body: paths,
    });
  }

  private uploadLocationUrl() {
    switch (environment.storageLocation) {
      case 'storage':
        return this.uploadUrl + '/firebase';
      case 'local':
        return this.uploadUrl;
      default:
        return this.uploadUrl;
    }
  }
}
