import { ClientSecretCredential } from '@azure/identity';
import { environment } from '../../environments/environment';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class MessagesService {
  // serviceBusClient = new ServiceBusClient(
  //   environment.azure.serviceBus.connectionString
  // );
  // namespace = `${environment.azure.serviceBus.namespace}.servicebus.windows.net`;
  // receiver = this.serviceBusClient.createReceiver(
  //   environment.azure.serviceBus.queueName
  // );

  clientCredential = new ClientSecretCredential(
    environment.azure.tenantId,
    environment.azure.clientId,
    environment.azure.clientSecret,
    {
      allowInsecureConnection: true,
    }
  );

  test() {
    this.clientCredential.getToken(environment.azure.scopes).then((res) => {
      console.log(res);
    });
  }

  // messageListener() {
  //   const myMessageHandler = async (message: any) => {
  //     // your code here
  //     alert(`message.body: ${message.body}`);
  //   };
  //   this.receiver.subscribe({
  //     processMessage: this.myMessageHandler,
  //     processError: this.myErrorHandler,
  //   });
  // }

  myMessageHandler = async (message: any) => {
    alert(`message.body: ${message.body}`);
  };
  myErrorHandler = async (args: any) => {
    alert(
      `Error occurred with ${args.entityPath} within ${args.fullyQualifiedNamespace}: ${args.error}`
    );
  };
}
