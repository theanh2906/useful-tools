import { inject, Injectable } from '@angular/core';
import { EventInput } from '@fullcalendar/common';
import { concatMap, map, toArray } from 'rxjs/operators';
import { BaseService } from './base.service';
import { from } from 'rxjs';
import { Database, get, ref, remove, set } from '@angular/fire/database';
import { v4 } from 'uuid';

@Injectable({
  providedIn: 'root',
})
export class EventService extends BaseService {
  database = inject(Database);
  eventsCollection = ref(this.database, 'events');

  get events() {
    return this._subject.asObservable();
  }

  getEvents = () => {
    return from(get(this.eventsCollection)).pipe(
      map((snapshot) => {
        if (snapshot.exists()) {
          const resData = snapshot.val();
          const eventInputs: EventInput[] = [];
          for (const key in resData) {
            if (resData.hasOwnProperty(key)) {
              eventInputs.push({
                id: key,
                allDay: resData[key].allDay,
                title: resData[key].title,
                start: resData[key].start,
                end: resData[key].end,
              });
            }
          }
          return eventInputs.sort(
            (a, b) =>
              this.utils.convertTime(b.start as string) -
              this.utils.convertTime(a.start as string)
          );
        }
        return [];
      })
    );
  };

  addEvent = (event: any) => {
    return from(set(ref(this.database, 'events/' + v4()), event));
  };

  editEvent = (event: any) => {
    return from(set(ref(this.database, 'events/' + event.id), event));
  };

  deleteEvents = (eventIds: string[]) => {
    return from(eventIds).pipe(
      concatMap(this.deleteEvent.bind(this)),
      toArray()
    );
  };

  deleteEvent = (eventId: string) => {
    return from(remove(ref(this.database, 'events/' + eventId)));
  };

  getEventsFromFirebase = () => {
    return this.http.get<any>(`${this.apiUrl}/events/firebase`).pipe;
    // tap(() => {
    //   this.store.dispatch(
    //     eventsAction({ actionType: ActionType.ADD, data: [] })
    //   );
    // });
  };
}
