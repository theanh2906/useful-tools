import { inject, Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { ReplaySubject } from 'rxjs';
import { gapi } from 'gapi-script';
import { AuthService } from '../auth/auth.service';
import { BaseService } from './base.service';
import { LoginMethod } from '../shared/models';

@Injectable({
  providedIn: 'root',
})
export class GoogleService extends BaseService {
  private auth2!: gapi.auth2.GoogleAuth;
  private _user = new ReplaySubject<gapi.auth2.GoogleUser | null>(1);
  private auth = inject(AuthService);

  constructor() {
    super();
    gapi.load('auth2', () => {
      this.auth2 = gapi.auth2.init({
        client_id: environment.google.oauth.clientId,
        cookie_policy: 'single_host_origin',
      });
    });
  }

  signIn() {
    this.auth2
      .signIn({
        fetch_basic_profile: true,
        scope: 'openid',
        ux_mode: 'redirect',
        redirect_uri: environment.google.oauth.redirectURI,
      })
      .then((user) => {
        if (user) {
          this._user.next(user);
          this.auth.login({
            googleUser: user,
            method: LoginMethod.GOOGLE,
            expiredIn: 0,
          });
        }
        this.activatedRoute.queryParams.subscribe((param) => {
          if (param.ref) {
            this.router.navigate(['/', param.ref]);
          } else {
            this.router.navigateByUrl('/calendar');
          }
        });
        // this.router.navigateByUrl('/calendar');
      })
      .catch((err) => {
        console.log(err);
        this._user.next(null);
      });
  }

  signOut() {
    this.auth2
      .signOut()
      .then(() => {
        this._user.next(null);
      })
      .catch(console.log);
  }
}
