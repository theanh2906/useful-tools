import { inject } from '@angular/core';
import { UtilsService } from './utils.service';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { BehaviorSubject, Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { NgxSpinnerService } from 'ngx-spinner';

export class BaseService {
  public data = new Observable<any>();
  public _subject = new BehaviorSubject<any>(null);
  protected utils = inject(UtilsService);
  protected http = inject(HttpClient);
  protected router = inject(Router);
  protected activatedRoute = inject(ActivatedRoute);
  protected store = inject(Store);
  protected spinner = inject(NgxSpinnerService);

  get apiUrl() {
    return this.utils.getApiUrl();
  }
}
