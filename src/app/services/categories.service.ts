import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { BaseService } from './base.service';

export interface Category {
  id: string;
  name: string;
  color: string;
}

@Injectable({
  providedIn: 'root',
})
export class CategoriesService extends BaseService {
  addCategory = (category: Category) =>
    this.http.post<any>(`${environment.server.cloudUrl}/categories.json`, {
      ...category,
      id: null,
    });

  deleteCategory = (id: string) =>
    this.http.delete(`${environment.server.cloudUrl}/categories/${id}.json`);

  editCategory = (category: Category) =>
    this.http.put(
      `${environment.server.cloudUrl}/categories/${category.id}.json`,
      {
        ...category,
        id: null,
      }
    );
}
