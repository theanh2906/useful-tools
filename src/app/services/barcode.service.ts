import { Injectable } from '@angular/core';
import { BaseService } from './base.service';

@Injectable({
  providedIn: 'root',
})
export class BarcodeService extends BaseService {
  generateQrCode(text: string) {
    return this.http.get<any>(`${this.apiUrl}/qr/image`, {
      params: {
        text,
      },
    });
  }
}
