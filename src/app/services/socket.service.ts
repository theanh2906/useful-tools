import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { environment } from '../../environments/environment';
import { MessageType } from '../shared/models';
import { io, Socket } from 'socket.io-client';
import { v4 } from 'uuid';

export interface Message {
  type?: string;
  data?: any;
}

export interface ChatMessage {
  type?: MessageType;
  sender?: string;
  content?: string;
}

@Injectable({
  providedIn: 'root',
})
export class SocketService {
  private _socket!: Socket;

  constructor() {
    this.connect();
    this.handleMessages();
    // this.connectBinance();
  }

  private _stompClient: any;

  get stompClient() {
    return this._stompClient;
  }

  private _sessionId = '';

  get sessionId() {
    return this._sessionId;
  }

  private _message = new Subject<Message>();

  get message() {
    return this._message.asObservable();
  }

  setSessionId(sessionId: string) {
    this._sessionId = sessionId;
  }

  setStompClient(stompClient: any) {
    this._stompClient = stompClient;
  }

  connect() {
    if (!this._socket || this._socket.disconnected) {
      this._socket = io(environment.wsEndpoint, {
        query: {
          userId: v4(),
        },
        ackTimeout: 10000,
        retries: 3,
      });
    }
  }

  // connectBinance() {
  //   if (!this._socket || this._socket.closed) {
  //     this._socket = webSocket({
  //       url: 'https://ws-api.binance.com:9443/ws-api/v3',
  //       openObserver: {
  //         next: () => {
  //           console.log('Connect to Binance successfully');
  //         },
  //         error: (err) => {
  //           console.log(err);
  //         },
  //       },
  //       closeObserver: {
  //         next: () => {
  //           console.log('Connection to Binance closed');
  //           this.connectBinance();
  //         },
  //       },
  //     });
  //   }
  // }

  disconnect() {
    if (this._socket) {
      this._socket.disconnect();
    }
  }

  emit(eventName: string, body?: string) {
    this._socket.emit(eventName, body);
  }

  sendMessage(message: string) {
    this._socket.emit('message', message);
  }

  on<T>(event: string): Observable<T> {
    return new Observable<T>((subscriber) => {
      this._socket.on(event, (data: T) => {
        subscriber.next(data);
      });
    });
  }

  handleMessages() {
    this._socket.on('ready', (message: any) => {
      console.log(message);
    });
  }
}
