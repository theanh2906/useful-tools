import { Injectable } from '@angular/core';
import { map, tap } from 'rxjs/operators';
import { BaseService } from './base.service';
import { Observable } from 'rxjs';
import { ActionType } from '../shared/models';
import { notesAction } from '../actions/app.action';

export interface Note {
  id: string;
  title: string;
  content: string;
  createdDate: number;
  modifiedDate?: number;
  isEdit?: boolean;
}

@Injectable({
  providedIn: 'root',
})
export class NotesService extends BaseService {
  get notes() {
    return this._subject.asObservable();
  }

  addNote = (note: any) => {
    if (this.utils.isCloud()) {
      return this.http.post<any>(`${this.apiUrl}/notes.json`, {
        ...note,
        id: null,
        isEdit: null,
      });
    } else {
      return this.http.post<any>(`${this.apiUrl}/notes/new`, {
        ...note,
        id: null,
        isEdit: null,
      });
    }
  };
  getAllNotes = () => {
    let getObs = new Observable<any>();
    if (this.utils.isCloud()) {
      getObs = this.http.get<any>(`${this.apiUrl}/notes.json`).pipe(
        map((resData) => {
          const notes: any[] = [];
          for (const key in resData) {
            if (resData.hasOwnProperty(key)) {
              notes.push({
                id: key,
                title: resData[key].title,
                content: resData[key].content,
                createdDate: resData[key].createdDate,
              });
            }
          }
          return notes.sort((a, b) => b.createdDate - a.createdDate);
        })
      );
    } else {
      getObs = this.http.get<Note[]>(`${this.apiUrl}/notes`);
    }
    return getObs;
  };
  deleteNote = (id: string) => {
    let delObs: Observable<any>;
    if (this.utils.isCloud()) {
      delObs = this.http.delete(`${this.apiUrl}/notes/${id}.json`);
    } else {
      delObs = this.http.delete(`${this.apiUrl}/notes/${id}`, {});
    }
    return delObs;
  };
  editNote = (note: any) => {
    let editObs = new Observable<any>();

    if (this.utils.isCloud()) {
      editObs = this.http.put(`${this.apiUrl}/notes/${note.id}.json`, {
        ...note,
        id: null,
      });
    } else {
      editObs = this.http.put(`${this.apiUrl}/notes`, {
        ...note,
      });
    }
    return editObs.pipe(
      tap(() =>
        this.store.dispatch(notesAction({ actionType: ActionType.EDIT }))
      )
    );
  };

  syncNotes() {
    return this.http
      .get<any>(`${this.apiUrl}/notes/firebase`)
      .pipe(
        tap(() =>
          this.store.dispatch(notesAction({ actionType: ActionType.FETCH }))
        )
      );
  }
}
