import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { WeatherInfo } from '../../commons/interfaces';
import { BaseService } from './base.service';

@Injectable({
  providedIn: 'root',
})
export class WeatherService extends BaseService {
  query = '';

  buildQuery(place: string) {
    this.query = this.utils.replacePlaceHolder(
      environment.visualCrossing.apiUrl,
      {
        place,
        apiKey: environment.visualCrossing.apiKey,
      }
    );
  }

  buildCoordsQuery(latitude: number, longitude: number) {
    this.query = this.utils.replacePlaceHolder(
      environment.visualCrossing.apiUrl,
      {
        place: latitude + ',' + longitude,
        apiKey: environment.visualCrossing.apiKey,
      }
    );
  }

  forecast() {
    return this.http.get<WeatherInfo>(this.query);
  }
}
