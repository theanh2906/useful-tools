import { Injectable, OnInit, signal } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class SignalsService implements OnInit {
  changeTextChoice = signal('');

  ngOnInit(): void {}
}
