import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class CachesService {
  private previousUrl = '';

  constructor() {}

  getPreviousUrl = () => {
    return this.previousUrl;
  };
  setPreviousUrl = (url: string) => {
    this.previousUrl = url;
  };
}
