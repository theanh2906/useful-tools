import { inject, Injectable } from '@angular/core';
import { AngularFireMessaging } from '@angular/fire/compat/messaging';
import { BaseService } from './base.service';
import { switchMap } from 'rxjs';
import { Messaging, onMessage } from '@angular/fire/messaging';

@Injectable({
  providedIn: 'root',
})
export class FirebaseService extends BaseService {
  afMessage = inject(AngularFireMessaging);
  messaging = inject(Messaging);

  requestPermission() {
    Notification.requestPermission().then((permission) => {
      console.log(permission);
      if (permission === 'granted') {
        this.afMessage.requestToken
          .pipe(
            switchMap((token) => {
              console.log(token);
              return this.subscribe(token);
            })
          )
          .subscribe({
            next: () => {
              console.log('Notification permission granted');
            },
            error: (err) => {
              console.log(err);
            },
          });
      }
    });
  }

  subscribe(token: string | null) {
    console.log(token);
    return this.http.post<any>(
      `${this.apiUrl}/notifications/subscribe`,
      token ?? ''
    );
  }

  listenForMessages() {
    onMessage(this.messaging, (payload) => {
      console.log(payload.data);
    });
  }
}
