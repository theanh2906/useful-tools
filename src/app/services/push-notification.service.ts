import { inject, Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { BaseService } from './base.service';
import { getToken, Messaging } from '@angular/fire/messaging';

@Injectable({
  providedIn: 'root',
})
export class PushNotificationService extends BaseService {
  http = inject(HttpClient);
  // swPush = inject(SwPush);
  messaging = inject(Messaging);

  // subscribeToNotifications() {
  //   if (this.swPush.isEnabled) {
  //     this.swPush
  //       .requestSubscription({
  //         serverPublicKey: environment.vapidPublicKey, // Your VAPID public key
  //       })
  //       .then((sub) => {
  //         console.log('Push Subscription:', sub);
  //         // this.sendSubscriptionToServer(sub).subscribe();
  //       })
  //       .catch((err) => {
  //         console.log(err);
  //       });
  //   }
  // }

  async getFcmToken(
    serviceWorkerRegistration: ServiceWorkerRegistration
  ): Promise<string> {
    return await getToken(this.messaging, {
      vapidKey: environment.vapidPublicKey,
      serviceWorkerRegistration,
    });
  }

  sendSubscriptionToServer(token: string) {
    return this.http.post(`${this.apiUrl}/notifications/subscribe`, { token });
  }
}
