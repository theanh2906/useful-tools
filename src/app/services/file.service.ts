import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';
import { BaseService } from './base.service';

export type UploadedFile = {
  name?: string;
  url?: string;
};

@Injectable({
  providedIn: 'root',
})
export class FileService extends BaseService {
  getAllUploadedFiles(): Observable<UploadedFile[]> {
    return this.http.get<UploadedFile[]>(`${environment.server.apiUrl}/files`);
  }

  getFileByName(name: string): Observable<UploadedFile> {
    return this.http.get<UploadedFile>(
      `${environment.server.apiUrl}/files/${name}`
    );
  }
}
