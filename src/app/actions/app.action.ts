import { createAction, props } from '@ngrx/store';
import { ActionType } from '../shared/models';
import { FileUpload } from 'primeng/fileupload';

// Change case
export const changeCase = createAction(
  'Change Text Case',
  props<{
    text: string;
    case: 'uppercase' | 'lowercase' | 'title' | 'camel' | 'kebab' | 'minify';
  }>()
);

// Data
export const fetchDataSuccess = createAction(
  'Fetch data success',
  props<{ data: any }>()
);

export const onRefresh = createAction(
  'Refresh page',
  props<{ refresh: boolean }>()
);

// Events
export const eventsAction = createAction(
  'Event action',
  props<{ actionType: ActionType; data: any }>()
);

// Notes
export const notesAction = createAction(
  'Note actions',
  props<{ actionType: ActionType }>()
);

export const getAllNotes = createAction('Get all notes');
export const deleteNote = createAction('Delete note', props<{ id: string }>());
export const syncFirebase = createAction('Sync note from firebase');
export const addNote = createAction('Add new note', props<{ note: any }>());

// Notification
export const notificationAction = createAction(
  'Add log',
  props<{ detail: string }>()
);

// Storage
export const deleteFiles = createAction(
  'Delete files',
  props<{ paths: string[] }>()
);

export const getAllFiles = createAction('[Storage] Get all files');
export const uploadFiles = createAction(
  'Upload files',
  props<{ formData: any; fileUpload: FileUpload }>()
);
