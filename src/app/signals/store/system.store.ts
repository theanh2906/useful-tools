import { patchState, signalStore, withMethods, withState } from '@ngrx/signals';

type SystemState = {
  pushSubscription: boolean;
};

const initialState: SystemState = {
  pushSubscription: false,
};

export const SystemStore = signalStore(
  { providedIn: 'root' },
  withState(initialState),
  withMethods((store) => ({
    checkForPushSubscription: (pushSubscription: boolean) => {
      patchState(store, { pushSubscription });
    },
  }))
);
