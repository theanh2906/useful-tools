import { ActionType } from '../../shared/models';
import {
  patchState,
  signalStore,
  withHooks,
  withMethods,
  withState,
} from '@ngrx/signals';
import { effect, inject } from '@angular/core';
import { StorageService } from '../../services/storage.service';
import { rxMethod } from '@ngrx/signals/rxjs-interop';
import { pipe, switchMap } from 'rxjs';
import { tapResponse } from '@ngrx/operators';
import { MessageService } from 'primeng/api';

type FileState = {
  action: ActionType;
  files: any[];
  location: string;
};

const initialState: FileState = {
  action: ActionType.NONE,
  files: [],
  location: '',
};

export const FilesStore = signalStore(
  { providedIn: 'root', protectedState: false },
  withState(initialState),
  withMethods(
    (
      store,
      storageService = inject(StorageService),
      messageService = inject(MessageService)
    ) => ({
      getAllFiles: rxMethod<void>(
        pipe(
          switchMap(() => {
            return storageService.getAllFiles().pipe(
              tapResponse({
                next: (res) => {
                  if (res) {
                    messageService.add({
                      key: 'appToast',
                      severity: 'success',
                      summary: 'File Uploaded',
                      detail: 'Successfully uploaded file',
                    });
                    patchState(store, { action: ActionType.FETCH, files: res });
                  }
                },
                error: () => {
                  messageService.add({
                    key: 'appToast',
                    severity: 'error',
                    summary: 'File Upload',
                    detail: 'Failed to upload file',
                  });
                },
              })
            );
          })
        )
      ),
      uploadFiles: rxMethod<FormData>(
        pipe(
          switchMap((data) => {
            return storageService.upload(data).pipe(
              tapResponse({
                next: () => {
                  patchState(store, { action: ActionType.ADD });
                },
                error: () => {},
              })
            );
          })
        )
      ),
      deleteFiles: rxMethod<string[]>(
        pipe(
          switchMap((paths) => {
            return storageService.deleteFiles(paths).pipe(
              tapResponse({
                next: () => {
                  patchState(store, { action: ActionType.REMOVE });
                },
                error: () => {},
              })
            );
          })
        )
      ),
    })
  ),
  withHooks({
    onInit(store) {
      store.getAllFiles();
      effect(() => {
        // if (store.action() === ActionType.ADD) {
        //   store.getAllFiles();
        // } else {
        //   setTimeout(() => {
        //     store.getAllFiles();
        //   }, 500);
        // }
      });
    },
  })
);
