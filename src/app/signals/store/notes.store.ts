import { ActionType } from '../../shared/models';
import { patchState, signalStore, withMethods, withState } from '@ngrx/signals';
import { inject } from '@angular/core';
import { Note, NotesService } from '../../services/notes.service';
import { rxMethod } from '@ngrx/signals/rxjs-interop';
import { pipe, switchMap } from 'rxjs';
import { tapResponse } from '@ngrx/operators';

type NoteState = {
  action: ActionType;
  notes: any[];
  noteToEdit: any;
};

const initialState: NoteState = {
  action: ActionType.NONE,
  notes: [],
  noteToEdit: null,
};

export const NotesStore = signalStore(
  { providedIn: 'root', protectedState: false },
  withState(initialState),
  withMethods((store, notesService = inject(NotesService)) => ({
    getNotes: rxMethod<void>(
      pipe(
        switchMap(() => {
          return notesService.getAllNotes().pipe(
            tapResponse({
              next: (res) => {
                patchState(store, { action: ActionType.FETCH, notes: res });
              },
              error: () => {},
            })
          );
        })
      )
    ),
    deleteNote: rxMethod<string>(
      pipe(
        switchMap((id) => {
          return notesService.deleteNote(id).pipe(
            tapResponse({
              next: () => {
                patchState(store, { action: ActionType.REMOVE });
              },
              error: () => {},
            })
          );
        })
      )
    ),
    addNote: rxMethod<Note>(
      pipe(
        switchMap((note) => {
          return notesService.addNote(note).pipe(
            tapResponse({
              next: () => {
                patchState(store, { action: ActionType.ADD });
              },
              error: () => {},
            })
          );
        })
      )
    ),
    editNote: (note: any) => {
      patchState(store, { noteToEdit: note });
    },
  }))
);
