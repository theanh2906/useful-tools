import {
  patchState,
  signalStore,
  withHooks,
  withMethods,
  withState,
} from '@ngrx/signals';
import { effect, inject } from '@angular/core';
import { EventService } from '../../services/event.service';
import { rxMethod } from '@ngrx/signals/rxjs-interop';
import { pipe, switchMap } from 'rxjs';
import { tapResponse } from '@ngrx/operators';
import { ActionType } from '../../shared/models';
import { EventInput } from '@fullcalendar/common';
import { MatSnackBar } from '@angular/material/snack-bar';

type EventState = {
  action: ActionType;
  events: any[];
  success: boolean;
  detail: string;
  dateToNavigate: number;
  timestamp: number;
};

const initialState: EventState = {
  action: ActionType.NONE,
  events: [],
  success: true,
  detail: '',
  dateToNavigate: new Date().getTime(),
  timestamp: new Date().getTime(),
};

export const EventStore = signalStore(
  { providedIn: 'root' },
  withState(initialState),
  withMethods(
    (
      store,
      eventService = inject(EventService),
      snackBar = inject(MatSnackBar)
    ) => ({
      getEvents: rxMethod<void>(
        pipe(
          switchMap(() => {
            return eventService.getEvents().pipe(
              tapResponse({
                next: (res) => {
                  patchState(store, { action: ActionType.FETCH, events: res });
                },
                error: () => {
                  patchState(store, {
                    action: ActionType.FETCH,
                    success: false,
                  });
                },
              })
            );
          })
        )
      ),
      addEvent: rxMethod<EventInput>(
        pipe(
          switchMap((event: EventInput) => {
            return eventService.addEvent(event).pipe(
              tapResponse({
                next: () => {
                  snackBar.open(
                    store.success()
                      ? 'Successfully added event'
                      : 'Failed to add event',
                    '',
                    {
                      duration: 2000,
                    }
                  );
                  patchState(store, { action: ActionType.ADD, success: true });
                },
                error: () => {
                  patchState(store, { action: ActionType.ADD, success: false });
                },
              })
            );
          })
        )
      ),
      deleteEvents: rxMethod<string[]>(
        pipe(
          switchMap((eventIds: string[]) => {
            return eventService.deleteEvents(eventIds).pipe(
              tapResponse({
                next: () => {
                  snackBar.open(
                    store.success()
                      ? `Successfully deleted events`
                      : `Failed to delete events`,
                    '',
                    {
                      duration: 2000,
                    }
                  );
                  patchState(store, {
                    action: ActionType.REMOVE,
                    success: true,
                  });
                },
                error: () => {
                  patchState(store, {
                    action: ActionType.REMOVE,
                    success: false,
                  });
                },
              })
            );
          })
        )
      ),
      editEvent: rxMethod<any>(
        pipe(
          switchMap((event: any) => {
            return eventService.editEvent(event).pipe(
              tapResponse({
                next: () => {
                  patchState(store, { action: ActionType.EDIT, success: true });
                },
                error: () => {
                  patchState(store, {
                    action: ActionType.EDIT,
                    success: false,
                  });
                },
              })
            );
          })
        )
      ),
      navigate: (date: string) => {
        patchState(store, {
          dateToNavigate: new Date(date).getTime(),
          timestamp: new Date().getTime(),
        });
      },
    })
  ),
  withHooks({
    onInit(store) {
      store.getEvents();
      effect(() => {
        if (
          [ActionType.ADD, ActionType.REMOVE, ActionType.EDIT].includes(
            store.action()
          )
        ) {
          store.getEvents();
        }
      });
    },
  })
);
