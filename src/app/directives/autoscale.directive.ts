import { Directive, ElementRef, inject, Input, OnInit } from '@angular/core';

@Directive({
  selector: '[autoscale]',
  standalone: true,
})
export class AutoscaleDirective implements OnInit {
  scaling = 1;
  elementRef = inject(ElementRef);
  @Input() reversed = false;

  ngOnInit(): void {
    this.onResize();
  }

  onResize() {
    const elWidth = this.elementRef.nativeElement.offsetWidth;
    const elHeight = this.elementRef.nativeElement.offsetHeight;
    const windowWidth = window.innerWidth;
    const windowHeight = window.innerHeight;
    this.scaling = this.reversed
      ? Math.max(windowWidth / elWidth, windowHeight / elHeight)
      : Math.max(elWidth / windowWidth, elHeight / windowHeight);
    this.elementRef.nativeElement.style.transform = `scale(${this.scaling})`;
  }
}
