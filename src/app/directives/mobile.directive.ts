import {
  Directive,
  ElementRef,
  HostListener,
  inject,
  Input,
  OnInit,
  Renderer2,
} from '@angular/core';
import { UtilsService } from '../services/utils.service';

export interface MobileConfig {
  style: { [key: string]: string };
  className: string;
  display: boolean;
}

@Directive({
  selector: '[mobileConfig]',
  standalone: true,
})
export class MobileDirective implements OnInit {
  @Input() mobileConfig!: Partial<MobileConfig>;
  originalStyle: any;
  private utils = inject(UtilsService);
  private element = inject(ElementRef<HTMLElement>);
  private renderer = inject(Renderer2);

  constructor() {
    this.originalStyle = this.element.nativeElement.style;
  }

  ngOnInit() {
    if (this.utils.isMobile && this.mobileConfig) {
      this.renderer.addClass(this.element.nativeElement, 'mobile');
      Object.entries(this.mobileConfig.style ?? {}).forEach(([key, value]) => {
        this.element.nativeElement.style.setProperty(key, value);
      });
    } else {
      this.renderer.removeClass(this.element.nativeElement, 'mobile');
    }
  }

  @HostListener('window:resize', ['$event']) windowsResize() {
    if (this.utils.isMobile) {
      this.renderer.addClass(this.element.nativeElement, 'mobile');
      Object.entries(this.mobileConfig.style ?? {}).forEach(([key, value]) => {
        this.element.nativeElement.style.setProperty(key, value);
      });
    } else {
      this.renderer.removeClass(this.element.nativeElement, 'mobile');
      Object.entries(this.originalStyle ?? {}).forEach(([key, value]) => {
        this.element.nativeElement.style.setProperty(key, value);
      });
    }
  }
}
