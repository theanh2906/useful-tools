import { AppState } from '../reducers';

export const selectNotes = (state: AppState) => state.data.data;
