export class StringUtils {
  static toTitleCase = (text: string) => {
    return text
      .split(' ')
      .map((word) => {
        return word.slice(0, 1).toUpperCase().concat(word.slice(1));
      })
      .join(' ');
  };

  static toCamelCase = (text: string) => {
    return text
      .split(' ')
      .map((word, index) => {
        if (index === 0) {
          return word;
        }
        return word.slice(0, 1).toUpperCase() + word.slice(1).toLowerCase();
      })
      .join('');
  };

  static toKebab = (text: string) => {
    return text
      .toLowerCase()
      .normalize('NFD')
      .replace(/\s/g, '-')
      .replace(/[̀-ͯ?!().',]/g, '');
  };

  static minify(text: string) {
    return text.replace(/\n/g, '').replace(/\s/g, '');
  }

  static capitalize(text: string) {
    const regex = /[!"#$%&'()*+,-./:;<=>?@[\]^_`{|}~]/g;
    text = text.replace(regex, ' ').trim();
    return this.toTitleCase(text);
  }

  static transform(text: string, action: string) {
    switch (action) {
      case 'uppercase':
        return text.toUpperCase();
      case 'lowercase':
        return text.toLowerCase();
      case 'title':
        return this.toTitleCase(text);
      case 'camel':
        return this.toCamelCase(text);
      case 'kebad':
        return this.toKebab(text);
      case 'minify':
        return this.minify(text);
      default:
        return text;
    }
  }
}
