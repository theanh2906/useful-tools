import { ApplicationConfig, importProvidersFrom } from '@angular/core';
import { provideRouter, withComponentInputBinding } from '@angular/router';
import { routes } from './app.routing';
import {
  HTTP_INTERCEPTORS,
  provideHttpClient,
  withInterceptorsFromDi,
} from '@angular/common/http';
import { provideStore } from '@ngrx/store';
import { reducers } from './reducers';
import { provideEffects } from '@ngrx/effects';
import { effects } from './effects';
import { FshareTokenInterceptor } from './interceptors/fshare-token.interceptor';
import { LoadingInterceptor } from './interceptors/loading.interceptor';
import { MessageService } from 'primeng/api';
import {
  BrowserAnimationsModule,
  NoopAnimationsModule,
} from '@angular/platform-browser/animations';
import { MatDialogModule } from '@angular/material/dialog';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { getDatabase, provideDatabase } from '@angular/fire/database';
import { initializeApp, provideFirebaseApp } from '@angular/fire/app';
import { environment } from '../environments/environment';
import { getMessaging, provideMessaging } from '@angular/fire/messaging';
import { AngularFireModule } from '@angular/fire/compat';

export const appConfig: ApplicationConfig = {
  providers: [
    provideRouter(routes, withComponentInputBinding()),
    importProvidersFrom(
      NoopAnimationsModule,
      BrowserAnimationsModule,
      MatDialogModule,
      MatSnackBarModule,
      AngularFireModule.initializeApp(environment.firebase as any)
    ),
    provideHttpClient(withInterceptorsFromDi()),
    // provideServiceWorker('firebase-messaging-sw.js', {
    //   enabled: true,
    //   registrationStrategy: 'registerImmediately',
    //   scope: 'firebase-cloud-messaging-push-scope',
    // }),
    provideStore(reducers),
    provideEffects(effects),
    {
      provide: HTTP_INTERCEPTORS,
      useClass: FshareTokenInterceptor,
      multi: true,
    },
    { provide: HTTP_INTERCEPTORS, useClass: LoadingInterceptor, multi: true },
    MessageService,
    provideFirebaseApp(() => initializeApp(environment.firebase as any)),
    provideDatabase(() => getDatabase()),
    provideMessaging(() => getMessaging()),
    // provideServiceWorker('ngsw-worker.js', {
    //   enabled: true,
    //   registrationStrategy: 'registerWhenStable:30000',
    // }),
    // {
    //   provide: HTTP_INTERCEPTORS,
    //   useClass: MsalInterceptor,
    //   multi: true,
    // },
    // {
    //   provide: MSAL_INSTANCE,
    //   useFactory: MSALInstanceFactory,
    // },
    // {
    //   provide: MSAL_GUARD_CONFIG,
    //   useFactory: MsalGuardConfigurationFactory,
    // },
    // MsalService,
    // MsalBroadcastService,
    // MsalGuard,
  ],
};
