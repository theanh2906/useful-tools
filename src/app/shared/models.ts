import GoogleUser = gapi.auth2.GoogleUser;

export enum MessageType {
  CHAT = 'CHAT',
  JOIN = 'JOIN',
  LEAVE = 'LEAVE',
}

export enum ActionType {
  FETCH = 'FETCH',
  ADD = 'ADD',
  REMOVE = 'REMOVE',
  SYNC = 'SYNC',
  EDIT = 'EDIT',
  CANCEL_EDITING = 'CANCEL_EDITING',
  NONE = 'NONE',
}

export interface AzureSSOResponse {
  access_token: string;
  expires_in: number;
  session_state: string;
  token_type: string;
}

export interface LoginCredential {
  email: string;
  password: string;
}

export enum LoginMethod {
  NONE = 0,
  CREDENTIAL = 1,
  GOOGLE = 2,
  AZURE = 3,
}

export interface AuthResponseData {
  kind?: string;
  idToken: string;
  email: string;
  refreshToken?: string;
  expiresIn: string;
  localId: string;
  registered?: boolean;
}

export interface AuthData {
  email: string;
  token: string;
  expiresIn: number;
  userId: string;
}

export interface LoginConfig {
  email?: string;
  password?: string;
  method?: LoginMethod;
  googleUser?: GoogleUser;
  expiredIn: number;
}

export interface FBLoginResponse {
  authResponse: {
    accessToken: string;
    data_access_expiration_time: number;
    expiresIn: number;
    graphDomain: string;
    signedRequest: string;
    userID: string;
  };
  status: string;
}

export enum Mode {
  NEW = 0,
  DELETE = -1,
  EDIT = 1,
  NONE = -2,
}

export const cssAttributeMapping: { [aviation: string]: string | string[] } = {
  w: 'width',
  h: 'height',
  d: 'display',
  ml: 'margin-left',
  mr: 'margin-right',
  mt: 'margin-top',
  mb: 'margin-bottom',
  my: ['margin-bottom', 'margin-top'],
  mx: ['margin-left', 'margin-right'],
  p: 'padding',
  pt: 'padding-top',
  pb: 'padding-bottom',
  pl: 'padding-left',
  pr: 'padding-right',
  px: ['padding-left', 'padding-right'],
  py: ['padding-top', 'padding-bottom'],
  lh: 'line-height',
  'max-w': 'max-width',
  'min-w': 'min-width',
  'max-h': 'max-height',
  'min-h': 'min-height',
};

export const cssMobileAttributeMapping: {
  [aviation: string]: string | string[];
} = {
  'm-w': 'width',
  'm-h': 'height',
  'm-d': 'display',
  'm-ml': 'margin-left',
  'm-mr': 'margin-right',
  'm-mt': 'margin-top',
  'm-mb': 'margin-bottom',
  'm-my': ['margin-bottom', 'margin-top'],
  'm-mx': ['margin-left', 'margin-right'],
  'm-pt': 'padding-top',
  'm-pb': 'padding-bottom',
  'm-pl': 'padding-left',
  'm-pr': 'padding-right',
  'm-px': ['padding-left', 'padding-right'],
  'm-py': ['padding-top', 'padding-bottom'],
  'm-lh': 'line-height',
  'm-max-w': 'max-width',
  'm-min-w': 'min-width',
  'm-max-h': 'max-height',
  'm-min-h': 'min-height',
};
