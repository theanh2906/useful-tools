self.addEventListener('push', (event) => {
  if (event.data) {
    const payload = event.data.json();
    event.waitUntil(
      self.registration.showNotification(payload.notification.title, {
        body: payload.notification.body,
        icon: payload.notification.icon || '/assets/icons/icon-192x192.png', // Fallback icon
      })
    );
  }
});

// Handle notification clicks (optional)
self.addEventListener('notificationclick', (event) => {
  event.notification.close();
  // Open a URL when the notification is clicked
  event.waitUntil(clients.openWindow(event.notification.data.url || '/')); // Replace '/' with your desired URL
});

self.addEventListener('periodicsync', (event) => {
  if (event.tag === 'fetch-updates') {
    event.waitUntil(fetchUpdates());
  }
});

async function fetchUpdates() {
  const response = await fetch('http://localhost:3000');
  const data = await response.json();
  console.log('Fetched updates:', data);
}
