importScripts(
  'https://www.gstatic.com/firebasejs/9.0.0/firebase-app-compat.js'
); // Or latest version
importScripts(
  'https://www.gstatic.com/firebasejs/9.0.0/firebase-messaging-compat.js'
); // Or latest version

// Initialize the Firebase app in the service worker
// Replace with your actual Firebase config
const firebaseConfig = {
  apiKey: 'AIzaSyBZKv3j_FmT_Nq33bmkF1MSBw2_ZAXPFSk',
  authDomain: 'useful-tools-api.firebaseapp.com',
  projectId: 'useful-tools-api',
  storageBucket: 'gs://useful-tools-api.firebasestorage.app',
  messagingSenderId: 740845971597,
  appId: '1:740845971597:web:08f7ebc11decc3c5425ac4',
};

firebase.initializeApp(firebaseConfig);

const messaging = firebase.messaging();

// messaging.onBackgroundMessage(function (payload) {
//   console.log('Background message received:', payload);
//   const notificationTitle = 'Title';
//   const notificationOptions = {
//     body: 'Body',
//     icon: '/assets/icons/icon-192x192.png',
//   };
//
//   self.registration.showNotification(notificationTitle, notificationOptions);
// });

self.addEventListener('sync', (event) => {
  console.log(event);
  if (event.tag === 'fetch-updates') {
    event.waitUntil(fetchUpdates());
  }
});

self.addEventListener('push', (event) => {
  if (event.data) {
    const payload = event.data.json().data;
    event.waitUntil(
      self.registration.showNotification(payload.title, {
        body: payload.body,
        icon: '/assets/icons/icon-192x192.png', // Fallback icon
      })
    );
  }
});

async function fetchUpdates() {
  console.log('aaa');
  const response = await fetch('http://localhost:3000');
  console.log(response);
  const data = await response.json();
  console.log('Fetched updates:', data);
}
