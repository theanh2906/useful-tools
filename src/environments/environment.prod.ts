export const environment = {
  openSocket: false,
  production: true,
  cloud: false,
  server: {
    cloudUrl: 'https://useful-tools-api-default-rtdb.firebaseio.com',
    apiUrl: 'https://backend.benna.life/api',
  },
  webApiKey: 'AIzaSyBZKv3j_FmT_Nq33bmkF1MSBw2_ZAXPFSk',
  fshare: {
    apiUrl: 'https://useful-tools-api.herokuapp.com',
  },
  facebook: {
    appId: '1070009906931041',
  },
  google: {
    oauth: {
      clientId:
        '740845971597-quolkmkd830njlgsl1cmpgk4q8pm5u5l.apps.googleusercontent.com',
      projectId: 'useful-tools-api',
      authURI: 'https://accounts.google.com/o/oauth2/auth',
      tokenURI: 'https://oauth2.googleapis.com/token',
      authProviderX509CertUrl: 'https://www.googleapis.com/oauth2/v1/certs',
      clientSecret: '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',
      javascriptOrigins: ['http://localhost:4200', 'https://benna.vercel.app'],
      redirectURI: 'https://benna.vercel.app',
    },
  },
  azure: {
    clientId: '48447683-68aa-45c3-beac-c613aee85234',
    clientSecret: '7ry8Q~jqQC1FL1R6V5.wh.rx7kZRG6dv-G-hOdhx',
    tenantId: '90d076c5-6610-4955-bccd-4f99ae488ef0',
    redirectURI: 'https://benna.vercel.app',
    scopes: [
      '.default',
      'openid',
      'email',
      'profile',
      'offline_access',
      'User.Read',
      'User.Read.All',
      'User.ReadBasic.All',
      'User.ReadWrite',
      'User.ReadWrite.All',
    ],
    ssoUrl:
      'https://login.microsoftonline.com/:tenantId/oauth2/v2.0/authorize?client_id=:clientId&response_type=token&redirect_uri=:redirectURI&scope=.default',
    serviceBus: {
      connectionString:
        'Endpoint=sb://theanh2906.servicebus.windows.net/;SharedAccessKeyName=RootManageSharedAccessKey;SharedAccessKey=RdikpQvetuZlTnU5V/XuAwEUszaNLzxTf+ASbNqbGAw=',
      namespace: 'theanh2906',
      queueName: 'benna',
    },
  },
  visualCrossing: {
    apiKey: 'W9ZMQH9J9C95VMW3EFA7XLNXB',
    apiUrl:
      'https://weather.visualcrossing.com/VisualCrossingWebServices/rest/services/timeline/:place?unitGroup=metric&key=:apiKey&contentType=json',
  },
  wsEndpoint: 'https://backend.benna.life',
  storageLocation: 'storage',
  firebase: {
    apiKey: 'AIzaSyBZKv3j_FmT_Nq33bmkF1MSBw2_ZAXPFSk',
    authDomain: 'useful-tools-api.firebaseapp.com',
    projectId: 'useful-tools-api',
    storageBucket: 'gs://useful-tools-api.firebasestorage.app',
    messagingSenderId: 740845971597,
    appId: '1:740845971597:web:08f7ebc11decc3c5425ac4',
  },
  vapidPublicKey:
    'BMfGjKkOd44sAPlyoU7a46Sux350X6Pl-6-7608_Fd0lwerYii8VfDYcwI-3iVEmexDMKXLXFBj_c9b17XZnhX0',
};
