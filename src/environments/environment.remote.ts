// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  server: {
    cloudUrl: 'https://useful-tools-api-default-rtdb.firebaseio.com',
    apiUrl: 'https://theanh2906.ddns.net/backend/api',
  },
  webApiKey: 'AIzaSyBZKv3j_FmT_Nq33bmkF1MSBw2_ZAXPFSk',
  fshare: {
    apiUrl: 'http://localhost:8081',
  },
  facebook: {
    appId: '1070009906931041',
  },
  google: {
    oauth: {
      clientId:
        '740845971597-quolkmkd830njlgsl1cmpgk4q8pm5u5l.apps.googleusercontent.com',
      projectId: 'useful-tools-api',
      authURI: 'https://accounts.google.com/o/oauth2/auth',
      tokenURI: 'https://oauth2.googleapis.com/token',
      authProviderX509CertUrl: 'https://www.googleapis.com/oauth2/v1/certs',
      clientSecret: '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',
      javascriptOrigins: ['http://localhost:4200', 'https://benna.vercel.app'],
      scope: {
        gmail: {
          readOnly: '',
        },
      },
      redirectURI: 'http://localhost:4200',
    },
  },
  azure: {
    clientId: '48447683-68aa-45c3-beac-c613aee85234',
    clientSecret: '7ry8Q~jqQC1FL1R6V5.wh.rx7kZRG6dv-G-hOdhx',
    tenantId: '90d076c5-6610-4955-bccd-4f99ae488ef0',
    redirectURI: '',
    scopes: [
      '.default',
      'openid',
      'email',
      'profile',
      'offline_access',
      'User.Read',
      'User.Read.All',
      'User.ReadBasic.All',
      'User.ReadWrite',
      'User.ReadWrite.All',
    ],
    ssoUrl:
      'https://login.microsoftonline.com/:tenantId/oauth2/v2.0/authorize?client_id=:clientId&response_type=token&redirect_uri=:redirectURI&scope=.default',
  },
  visualCrossing: {
    apiKey: 'W9ZMQH9J9C95VMW3EFA7XLNXB',
    apiUrl:
      'https://weather.visualcrossing.com/VisualCrossingWebServices/rest/services/timeline/:place?unitGroup=metric&key=:apiKey&contentType=json',
  },
  wsEndpoint: 'https://theanh2906.ddns.net/backend/websocket/api',
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
